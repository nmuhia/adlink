from django.db import models
import datetime

DURATION = (
    ('Days','Days'),
    ('Weeks','Weeks'),
    ('Months','Months'),
)
SPACE_SIZE = (
    ('A4P', 'A4 Potrait'),
    ('A4L', 'A4 Landscape'),
)
SPACE_DIMENSION = (
    #SPACE DESCRIPTION, WIDTH, HEIGHT in mm
    ('A1P','594','840'),
    ('A1L','840','594'),
    ('A2P','420','594'),
    ('A2L','594','420'),
    ('A3P','297','420'),
    ('A3L','420','297'),
    ('A4P','210','297'),
    ('A4L','297','210'),
    ('A5P','148','210'),
    ('A5L','210','148'),
)

CURRENCIES = (
    ('KES', 'Kenyan Shilling'),
    ('TZS', 'Tanzanian Shilling'),
    ('UGX', 'Ugandan Shilling'),
    #add list of all currencies here -- continue adding with expansion
)
RATES = (
    ('Daily','Daily'),
    ('Weekly','Weekly'),
    ('Monthly','Monthly'),
    ('Yearly','Yearly'),
)

SECTION_SHAPE = (
    ('Square', 'Square'),
    ('Rectangle', 'Rectange'),
    ('Polygon', 'Polygon'),
)
RESERVATION_STATUS = (
    ('R', 'Reserved'),
    ('C', 'Cancelled'),
    ('B', 'Booked'),
    ('E', 'Expired'),
)

class Location(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500, blank=True, null=True)
    traffic = models.CharField(max_length=100, blank=True, null=True)
    logo = models.FileField(upload_to='locations', blank=True, null=True)
    img1 = models.FileField(upload_to='locations', blank=True, null=True)
    img2 = models.FileField(upload_to='locations', blank=True, null=True)
    currency = models.CharField(max_length=100, blank=True, choices=CURRENCIES ,default="KES")
    tax_rate = models.CharField(max_length=10, blank=True, null=True, default=0)

    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Location"
        ordering = ['name']


class Section(models.Model):
    from Adverts.models import Category
    location = models.ForeignKey(Location, blank=False, null=False)
    label = models.CharField(max_length=100, blank=False, null=False)
    shape = models.CharField(max_length=100, choices=SECTION_SHAPE, blank=False, null=False, default="Rectangle")
    dimensions = models.CharField(max_length=100, blank=False, null=False)
    image = models.FileField(upload_to='locations', blank=True, null=True)
    category = models.ForeignKey(Category, blank=True, null=True)
    
    def __unicode__(self):
        return self.label

    class Meta:
        verbose_name = "Section"

class Space(models.Model):
    from Adverts.models import AD_SIZE, AD_ORIENTATION
    space_label = models.CharField(max_length=100, blank=False, null=False, default="0")
    section = models.ForeignKey(Section, blank=False, null=False, default=0)    
    size = models.CharField(max_length=100, choices=AD_SIZE, blank=False, null=False, default="A4")
    orientation = models.CharField(max_length=100, choices=AD_ORIENTATION, blank=False, null=False, default="Portrait")
    row = models.CharField(max_length=10, blank=True, null=True)
    col = models.CharField(max_length=10, blank=True, null=True)

    def __unicode__(self):
        return self.space_label

    class Meta:
        verbose_name = "Space"
        ordering = ['section', 'row', 'col']

class Inventory(models.Model):
    from Adverts.models import Category, AD_SIZE, AD_ORIENTATION    
    receipt_no = models.CharField(max_length=100, blank=False, null=False)
    first_name = models.CharField(max_length=100, blank=False, null=False)
    last_name = models.CharField(max_length=100, blank=False, null=False)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    email_address = models.CharField(max_length=100, blank=True, null=True)
    ad_size = models.CharField(max_length=100, choices=AD_SIZE, blank=True, null=True)
    ad_orientation = models.CharField(max_length=100, choices=AD_ORIENTATION, blank=True, null=True)
    ad_category = models.ForeignKey('Adverts.Category', blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField()
    upload_date = models.DateField(auto_now_add=True)
    section = models.ForeignKey(Section, blank=True, null=True)

    def __unicode__(self):
        return self.receipt_no + " - "+ self.ad_size + " - "+ self.ad_orientation

    class Meta:
        verbose_name = "Inventory"

class Space_Booking(models.Model):
    from Adverts.models import Booked_Advert
    space = models.ForeignKey(Space, blank=False, null=False)
    advert = models.ForeignKey(Booked_Advert, blank=True, null=True)
    inventory = models.ForeignKey(Inventory, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    status = models.CharField(max_length=100, choices=RESERVATION_STATUS, blank=False, null=False, default="C")

    def __unicode__(self):
        return self.status

    class Meta:
        verbose_name = "Space Booking"
        ordering = ['start_date']
    

class Rate(models.Model):
    location = models.ForeignKey(Location, blank=False, null=False)
    daily_rate_a1 = models.CharField(max_length=100, blank=True, null=True, default=0)
    daily_rate_a2 = models.CharField(max_length=100, blank=True, null=True, default=0)
    daily_rate_a3 = models.CharField(max_length=100, blank=True, null=True, default=0)
    daily_rate_a4 = models.CharField(max_length=100, blank=True, null=True, default=0)
    daily_rate_a5 = models.CharField(max_length=100, blank=True, null=True, default=0)
    weekly_rate_a1 = models.CharField(max_length=100, blank=True, null=True, default=0)
    weekly_rate_a2 = models.CharField(max_length=100, blank=True, null=True, default=0)
    weekly_rate_a3 = models.CharField(max_length=100, blank=True, null=True, default=0)
    weekly_rate_a4 = models.CharField(max_length=100, blank=True, null=True, default=0)
    weekly_rate_a5 = models.CharField(max_length=100, blank=True, null=True, default=0)
    monthly_rate_a1 = models.CharField(max_length=100, blank=True, null=True, default=0)
    monthly_rate_a2 = models.CharField(max_length=100, blank=True, null=True, default=0)
    monthly_rate_a3 = models.CharField(max_length=100, blank=True, null=True, default=0)
    monthly_rate_a4 = models.CharField(max_length=100, blank=True, null=True, default=0)
    monthly_rate_a5 = models.CharField(max_length=100, blank=True, null=True, default=0)
    yearly_rate_a1 = models.CharField(max_length=100, blank=True, null=True, default=0)
    yearly_rate_a2 = models.CharField(max_length=100, blank=True, null=True, default=0)
    yearly_rate_a3 = models.CharField(max_length=100, blank=True, null=True, default=0)
    yearly_rate_a4 = models.CharField(max_length=100, blank=True, null=True, default=0)
    yearly_rate_a5 = models.CharField(max_length=100, blank=True, null=True, default=0)

    def __unicode__(self):
        return self.daily_rate_a1

    class Meta:
        verbose_name = "Rate"
