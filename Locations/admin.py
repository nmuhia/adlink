from django.contrib import admin
from models import Location, Section, Space, Space_Booking, Inventory, Rate

class LocationAdmin(admin.ModelAdmin):
    list_display = ('name',)

class SectionAdmin(admin.ModelAdmin):
    list_display = ('location','label','shape','dimensions',)

class RateAdmin(admin.ModelAdmin):
    list_display = ('location','daily_rate_a1','weekly_rate_a1','monthly_rate_a1','yearly_rate_a1',)

class SpaceAdmin(admin.ModelAdmin):
    list_display = ('space_label','row','col','size','orientation','section',)

class SpaceBookingAdmin(admin.ModelAdmin):
    list_display = ('advert','space','start_date','end_date','status',)

class InventoryAdmin(admin.ModelAdmin):
    list_display = ('receipt_no','first_name','last_name','ad_size','ad_orientation',)

admin.site.register(Location, LocationAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Space, SpaceAdmin)
admin.site.register(Space_Booking, SpaceBookingAdmin)
admin.site.register(Rate, RateAdmin)
admin.site.register(Inventory, InventoryAdmin)
