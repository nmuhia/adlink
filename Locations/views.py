from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404

from django.db.models import Q

from Profiles.models import User_Profile

from models import Location, Section, Rate, Inventory, Space, Space_Booking, SPACE_SIZE, CURRENCIES, SECTION_SHAPE, SPACE_DIMENSION
from Adverts.models import Advert, Booked_Advert, Category, Error_Log, AD_SIZE, AD_ORIENTATION, AD_SIZE, AD_PAPER_TYPE

import csv
import json

import datetime
from datetime import date, timedelta, datetime
from django.utils import timezone
from django.utils.timezone import now,utc

"""
SECTION ON LOCATION MANAGEMENT
"""

@login_required
def locations_page(request):    
    try:
        template_context = {}
        if request.user.is_staff:        
            template_context['locations'] = Location.objects.all()
            template_context['currencies'] = CURRENCIES
            template_context['sizes'] = AD_SIZE

            return render(request,"staff/locations.html",template_context)            
        else:
            return redirect("/staff/logout/")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Locations Page: "+str(e))
        return HttpResponse("Locations: "+str(e))

@login_required
def add_location_form(request):
    template_context = {}
    try:
        template_context['sizes'] = AD_SIZE
        template_context['currencies'] = CURRENCIES
        return render(request, "staff/add_location.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Create Location Form: "+str(e))
        return HttpResponse("error")


@login_required
def add_location(request): 
    name = request.POST['name']
    tax_rate = request.POST['tax_rate']
    currency = request.POST['currency']
    traffic = request.POST['traffic']
    description = request.POST['description']

    if tax_rate == "":
        tax_rate = 0
    
    try:
        check_exists = Location.objects.filter(name=name)

        if check_exists:
            return HttpResponse("exists")
        else:
            location = Location.objects.create(name=name, tax_rate=tax_rate, currency=currency, traffic=traffic, description=description)
            rate = Rate.objects.create(location=location)
            rate.daily_rate_a1 = request.POST['A1_daily_rate']
            rate.daily_rate_a2 = request.POST['A2_daily_rate']
            rate.daily_rate_a3 = request.POST['A3_daily_rate']
            rate.daily_rate_a4 = request.POST['A4_daily_rate']
            rate.daily_rate_a5 = request.POST['A5_daily_rate']
            rate.weekly_rate_a1 = request.POST['A1_weekly_rate']
            rate.weekly_rate_a2 = request.POST['A2_weekly_rate']
            rate.weekly_rate_a3 = request.POST['A3_weekly_rate']
            rate.weekly_rate_a4 = request.POST['A4_weekly_rate']
            rate.weekly_rate_a5 = request.POST['A5_weekly_rate']
            rate.monthly_rate_a1 = request.POST['A1_monthly_rate']
            rate.monthly_rate_a2 = request.POST['A2_monthly_rate']
            rate.monthly_rate_a3 = request.POST['A3_monthly_rate']
            rate.monthly_rate_a4 = request.POST['A4_monthly_rate']
            rate.monthly_rate_a5 = request.POST['A5_monthly_rate']
            rate.yearly_rate_a1 = request.POST['A1_yearly_rate']
            rate.yearly_rate_a2 = request.POST['A2_yearly_rate']
            rate.yearly_rate_a3 = request.POST['A3_yearly_rate']
            rate.yearly_rate_a4 = request.POST['A4_yearly_rate']
            rate.yearly_rate_a5 = request.POST['A5_yearly_rate']

            rate.save()

            #upload logo
            try:
                logo = request.FILES['logo']
                location.logo = logo
                location.save()
            except Exception as e:
                pass
            #upload img1
            try:
                img1 = request.FILES['img1']
                location.img1 = img1
                location.save()
            except Exception as e:
                pass
            #upload img2
            try:
                logo = request.FILES['img2']
                location.img2 = img2
                location.save()
            except Exception as e:
                pass

        
        return redirect("/staff/locations/")               
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Location: "+str(e))
        return redirect("/staff/locations/")               

@login_required
def edit_location_form(request,location_id):
    template_context = {}


    try:
        location = Location.objects.get(id=location_id)
        rate = Rate.objects.get(location=location)
        template_context['location'] = location
        template_context['currencies'] = CURRENCIES

        #daily rates        
        template_context['a1_daily_rate'] = rate.daily_rate_a1
        template_context['a2_daily_rate'] = rate.daily_rate_a2
        template_context['a3_daily_rate'] = rate.daily_rate_a3
        template_context['a4_daily_rate'] = rate.daily_rate_a4
        template_context['a5_daily_rate'] = rate.daily_rate_a5

        #weekly rates
        template_context['a1_weekly_rate'] = rate.weekly_rate_a1
        template_context['a2_weekly_rate'] = rate.weekly_rate_a2
        template_context['a3_weekly_rate'] = rate.weekly_rate_a3
        template_context['a4_weekly_rate'] = rate.weekly_rate_a4
        template_context['a5_weekly_rate'] = rate.weekly_rate_a5

        #monthly rates
        template_context['a1_monthly_rate'] = rate.monthly_rate_a1
        template_context['a2_monthly_rate'] = rate.monthly_rate_a2
        template_context['a3_monthly_rate'] = rate.monthly_rate_a3
        template_context['a4_monthly_rate'] = rate.monthly_rate_a4
        template_context['a5_monthly_rate'] = rate.monthly_rate_a5

        #yearly
        template_context['a1_yearly_rate'] = rate.yearly_rate_a1
        template_context['a2_yearly_rate'] = rate.yearly_rate_a2
        template_context['a3_yearly_rate'] = rate.yearly_rate_a3
        template_context['a4_yearly_rate'] = rate.yearly_rate_a4
        template_context['a5_yearly_rate'] = rate.yearly_rate_a5

        return render(request,"staff/edit_location.html",template_context)        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit Location: "+str(e))
        return HttpResponse("error")

@login_required
def edit_location(request):
    print "edit location"
    location_id = request.POST['location_id']
    location_name = request.POST['name']

    print location_id
    print location_name

    try:
        #check if the name already exissts
        check_exists = Location.objects.filter(name=location_name)
        check_exists = check_exists.exclude(id=location_id)

        if check_exists:
            return HttpResponse("exists")
        else:
            
            tax_rate = request.POST['tax_rate']
            currency = request.POST['currency']            
            
            location = Location.objects.get(id=location_id)
            rate = Rate.objects.get(location=location)

            rate.daily_rate_a1 = request.POST['A1_daily_rate']
            rate.daily_rate_a2 = request.POST['A2_daily_rate']
            rate.daily_rate_a3 = request.POST['A3_daily_rate']
            rate.daily_rate_a4 = request.POST['A4_daily_rate']
            rate.daily_rate_a5 = request.POST['A5_daily_rate']
            rate.weekly_rate_a1 = request.POST['A1_weekly_rate']
            rate.weekly_rate_a2 = request.POST['A2_weekly_rate']
            rate.weekly_rate_a3 = request.POST['A3_weekly_rate']
            rate.weekly_rate_a4 = request.POST['A4_weekly_rate']
            rate.weekly_rate_a5 = request.POST['A5_weekly_rate']
            rate.monthly_rate_a1 = request.POST['A1_monthly_rate']
            rate.monthly_rate_a2 = request.POST['A2_monthly_rate']
            rate.monthly_rate_a3 = request.POST['A3_monthly_rate']
            rate.monthly_rate_a4 = request.POST['A4_monthly_rate']
            rate.monthly_rate_a5 = request.POST['A5_monthly_rate']
            rate.yearly_rate_a1 = request.POST['A1_yearly_rate']
            rate.yearly_rate_a2 = request.POST['A2_yearly_rate']
            rate.yearly_rate_a3 = request.POST['A3_yearly_rate']
            rate.yearly_rate_a4 = request.POST['A4_yearly_rate']
            rate.yearly_rate_a5 = request.POST['A5_yearly_rate']

            location.name = location_name
            location.currency = currency
            location.tax_rate = tax_rate

            try:
                logo = request.FILES['logo']
                location.logo = logo                
            except Exception as e:
                pass

            location.save()
            rate.save()
            
            return redirect("/staff/locations/")               

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit Location: "+str(e))
        return HttpResponse("error")



@login_required
def delete_location(request):
    location_id = request.POST["location_id"]
    try:
        location = Location.objects.get(id=location_id)
        location.de_lete()
        return HttpResponse("deleted")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Delete Location: "+str(e))
        return HttpResponse("error")

def show_daily_rates():
    locations_list = []

    for location in Location.objects.all():
        rate = Rate.objects.get(location=location)
        location_info = {}
        location_info['location'] = location
        location_info['name'] = location.name
        location_info['a1_price'] = str(rate.daily_rate_a1)
        location_info['a2_price'] = str(rate.daily_rate_a2)
        location_info['a3_price'] = str(rate.daily_rate_a3)
        location_info['a4_price'] = str(rate.daily_rate_a4)
        location_info['a5_price'] = str(rate.daily_rate_a5)

        locations_list.append(location_info)

    return locations_list

def show_weekly_rates():
    locations_list = []

    for location in Location.objects.all():
        rate = Rate.objects.get(location=location)
        location_info = {}
        location_info['location'] = location
        location_info['name'] = location.name
        location_info['a1_price'] = str(rate.weekly_rate_a1)
        location_info['a2_price'] = str(rate.weekly_rate_a2)
        location_info['a3_price'] = str(rate.weekly_rate_a3)
        location_info['a4_price'] = str(rate.weekly_rate_a4)
        location_info['a5_price'] = str(rate.weekly_rate_a5)

        locations_list.append(location_info)

    return locations_list

def show_monthly_rates():
    locations_list = []

    for location in Location.objects.all():
        rate = Rate.objects.get(location=location)
        location_info = {}
        location_info['location'] = location
        location_info['name'] = location.name
        location_info['a1_price'] = str(rate.monthly_rate_a1)
        location_info['a2_price'] = str(rate.monthly_rate_a2)
        location_info['a3_price'] = str(rate.monthly_rate_a3)
        location_info['a4_price'] = str(rate.monthly_rate_a4)
        location_info['a5_price'] = str(rate.monthly_rate_a5)

        locations_list.append(location_info)

    return locations_list

def show_yearly_rates():
    locations_list = []

    for location in Location.objects.all():
        rate = Rate.objects.get(location=location)
        location_info = {}
        location_info['location'] = location
        location_info['name'] = location.name
        location_info['a1_price'] = str(rate.yearly_rate_a1)
        location_info['a2_price'] = str(rate.yearly_rate_a2)
        location_info['a3_price'] = str(rate.yearly_rate_a3)
        location_info['a4_price'] = str(rate.yearly_rate_a4)
        location_info['a5_price'] = str(rate.yearly_rate_a5)

        locations_list.append(location_info)

    return locations_list

@login_required
def sections_page(request):
    """
        test sections page, divide it into different areas
    """
    template_context = {}
    try:
        template_context['sections'] = Section.objects.all().order_by('location')
        template_context['locations'] = Location.objects.all()
        template_context['inventory'] = Inventory.objects.all()
        return render(request, "staff/sections.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Sections Page: "+str(e))
        return HttpResponse("error")

@login_required
def add_section_form(request):
    template_context = {}
    try:
        template_context['shapes'] = SECTION_SHAPE
        template_context['locations'] = Location.objects.all()
        return render(request, "staff/add_section.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Section Form: "+str(e))
        return HttpResponse("error")

@login_required
def add_section(request):
    try:
        label = request.POST['label']
        dimensions = request.POST['dimensions']
        location_id = request.POST['location']
        shape = request.POST['shape']        
        location = Location.objects.get(id=location_id)

        section = Section.objects.create(label=label, dimensions=dimensions, location=location, shape=shape)

        #upload image
        try:
            img = request.FILES['img']
            section.image = img
            section.save()
        except Exception as e:
            pass
        
        return redirect("/staff/sections/")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Create Section: "+str(e))   
        return redirect("/staff/sections/")

@login_required
def manage_spaces_page(request, section_id):
    template_context = {}    
    try:
        section = Section.objects.get(id=section_id)
        template_context['spaces'] = Space.objects.filter(section=section).order_by("space_label")
        template_context['section'] = section        
        template_context['orientation'] = AD_ORIENTATION
        template_context['sizes'] = AD_SIZE
        return render(request, "staff/spaces.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Manage Spaces Page: "+str(e))
        return render(request, "staff/spaces.html")

@login_required
def add_space(request):
    try:
        space_label = request.POST['space_label']
        row = request.POST['space_row']
        col = request.POST['space_col']
        section_id = request.POST['section']
        orientation = request.POST['orientation']
        size = request.POST['size']

        section = Section.objects.get(id=section_id)

        check_exists = Space.objects.filter(section=section, row=row, col=col)
        if len(check_exists) > 0:
            return HttpResponse("exists")
        else:            
            Space.objects.create(space_label=space_label, row=row, col=col, section=section, size=size, orientation=orientation)
            return HttpResponse("success")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Space: "+str(e))
        return HttpResponse(e)


@login_required
def delete_reservations(request):
    location_id = request.POST['location_id']
    start_date = request.POST['start_date']
    end_date = request.POST['end_date']
    quantity = request.POST['quantity']
    ad_id = request.POST['ad_id']

    print start_date
    print end_date

    try:        
        location = Location.objects.get(name=location_id)
        advert = Advert.objects.get(id=ad_id)
        booked_adverts = Booked_Advert.objects.filter(advert=advert, location=location, start_date=start_date, end_date=end_date, quantity=quantity)
        if booked_adverts.count() > 0:
            num_quantity = int(quantity)
            for ad in booked_adverts:
                if num_quantity > 0:
                    ad.delete()
                    num_quantity = num_quantity - 1
                else:
                    break                
            return HttpResponse(1)
        else:
            return HttpResponse(0)
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Available Spaces: "+str(e))
        return HttpResponse(0)


@login_required
def available_spaces(request):
    print "it comes to here"
    location_name = request.POST['location_id']
    size = request.POST['size']
    orientation = request.POST['orientation']
    quantity = request.POST['quantity']
    start_date = request.POST['start_date']
    end_date = request.POST['end_date']
    ad_id = request.POST['ad_id']    

    try:        
        category = Advert.objects.get(id=ad_id).category
        location = Location.objects.get(name=location_name)
        spaces = check_num_spaces(location.id, size, orientation, quantity, start_date, end_date, category.id, True, ad_id,None)
        return HttpResponse(spaces)
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Available Spaces: "+str(e))
        return HttpResponse(0)

def check_spaces(request, is_categorized=False, category=None):
    """
    ==============================================================================
        A space can either be in A4L or A4P
        1. A1P - 8 A4P
        2. A1L - 8 A4L
        3. A2P - 4 A4P
        4. A2L - 4 A4L
        5. A3P - 2 A4P
        6. A3L - 2 A4L
        7. A4P - 1 A4P
        8. A4L - 1 A4L
        9. A5P - 1/2 A4P
        10. A5L - 1/2 A4L
    ==============================================================================    
    """
    list_1 = ("Yaya", "A4", "Portrait", "7")
    list_2 = ("Yaya", "A1", "Landscape", "1","2015-12-10", "2016-01-01")
    list_3 = ("Yaya", "A2", "Landscape", "1")
    list_4 = ("Yaya", "A3", "Portrait", "1")

    try:
        #check list 2
        name = list_2[0]
        size = list_2[1]
        orientation = list_2[2]
        quantity = int(list_2[3])
        start_date = list_2[4]
        end_date = list_2[5]
        

        location = Location.objects.get(name=name)
        sections = Section.objects.filter(location=location)
        if is_categorized:
            category = Category.objects.all()[0]#use first category for now
            sections = sections.filter(category=category)

        ad_width = 0
        ad_height = 0

        ad_size = str(size)+""+str(orientation[:1])

        ad_dimension = [s for s in SPACE_DIMENSION if s[0] == ad_size ]
        ad_width = int(ad_dimension[0][1])
        ad_height = int(ad_dimension[0][2])

        #divide by A4 portrait, A4 landscape
        A4P_width = int([s for s in SPACE_DIMENSION if s[0] == "A4P" ][0][1])
        A4P_height = int([s for s in SPACE_DIMENSION if s[0] == "A4P" ][0][2])

        A4L_width = int([s for s in SPACE_DIMENSION if s[0] == "A4L" ][0][1])
        A4L_height = int([s for s in SPACE_DIMENSION if s[0] == "A4L" ][0][2])        

        A4L_use = False
        A4P_use = False

        #compare ad size and height with space, check what spaces I need to check if A4L/A4P
        A4L_width_ratio = float(ad_width / A4L_width)
        A4L_height_ratio = float(ad_height / A4L_height)

        A4P_width_ratio = float(ad_width / A4P_width)
        A4P_height_ratio = float(ad_height / A4P_height)

        if float(ad_width % A4L_width) == 0:
            A4L_use = True

        elif float(ad_width % A4P_width) == 0:
            A4P_use = True

        all_spaces = Space.objects.filter(section=sections[1]) #only one section
        if A4L_use:
            all_spaces = all_spaces.filter(size="A4", orientation="Landscape").order_by('row','col')

            for space in all_spaces:
                booked_spaces = Space_Booking.objects.filter(space=space).filter(Q(status="B") | Q(status="R"))
                for bs in booked_spaces:
                    bs_start_date = bs.start_date
                    bs_end_date = bs.end_date

                    #check for overlap
                    if(start_date <= bs_end_date and bs_start_date <= end_date):
                        all_spaces = all_spaces.exclude(id=space.id)
                        break

        elif A4P_use:
            all_spaces = all_spaces.filter(size="A4", orientation="Portrait").order_by('row','col')
            for space in all_spaces:
                booked_spaces = Space_Booking.objects.filter(space=space).filter(Q(status="B") | Q(status="R"))
                for bs in booked_spaces:
                    bs_start_date = bs.start_date
                    bs_end_date = bs.end_date

                    #check for overlap
                    if(start_date <= bs_end_date and bs_start_date <= end_date):
                        all_spaces = all_spaces.exclude(id=space.id)
                        break

        #check the number of spaces I need to make to the right and bottom
        #check if there is enough space in the bottom and enough space in the right
        next_row = all_spaces[0].row
        next_col = all_spaces[0].col
        for sp in all_spaces:
            #this space
            #check the size
            #row = sp.row
            #col = sp.col

            width_steps = 0
            height_steps = 0

            if A4P_use:
                width_steps = A4P_width_ratio - 1
                height_steps = A4P_height_ratio - 1
            elif A4L_use:
                width_steps = A4L_width_ratio - 1
                height_steps = A4L_height_ratio - 1
        
            #for i in range(0, width_steps):
            #check if there's empty space below
            bottom_available = False
            right_available = False

            for i in range(0, int(height_steps)):
                next_sp = Space.objects.filter(row=(int(next_row)+1+i), col=sp.col, size=sp.size, orientation=sp.orientation)
                if next_sp.count() > 0:
                    bottom_available = True
                else:
                    bottom_available = False
                    break

                print next_sp

            if bottom_available:
                for i in range(0, int(width_steps)):
                    next_sp = Space.objects.filter(col=(int(next_col)+1+i), row=sp.row, size=sp.size, orientation=sp.orientation)
                    if next_sp.count() > 0:
                        right_available = True
                    else:
                        right_available = False
                        break

                    print next_sp

            if right_available: #both right and bottom are available, assign the space to the AD
                print "both right and bottom available"
            #print right_available

            else:
                print "no space available"


            next_col = int(sp.row) + int(width_steps)
            next_row = int(sp.col) + int(height_steps)

            print next_col
            print next_row
            print "********************************************8"
            #print bottom_available
            #check if the number of rows extend to the right
            #print width_steps
            #print height_steps
            
            


        for i in range(0, quantity):
            #get width and size
            print "list 2"     

        return HttpResponse(list_1[0])
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Check Spaces: "+str(e))
        return HttpResponse("error: "+str(e))

    """section = Section.objects.all()
    print section
    print "(((((WELCOME)))))"
    if size == "A4" and orientation == "Landscape":
        #check if there is any space that can fit this size
    elif size == "A4" and orientation == 
    #for section in 
    #for space in Space.objects.filter(section__location=location)"""

    
       
    


def check_num_spaces(location_id, size, orientation, quantity, start_date, end_date, category_id, booking=False, ad_id=None, inv_id=None):
    """
        This is for checking available spaces that can handle the booked AD or added inventory
    """
    try:
        num_spaces = 0
        location = Location.objects.get(id=location_id)   
        category = Category.objects.get(id=category_id)
        sections = Section.objects.filter(location=location, category=category)


        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        start_date = timezone.make_aware(start_date,timezone=utc)
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
        end_date = timezone.make_aware(end_date,timezone=utc)
        start_date = start_date.date()
        end_date = end_date.date()

        print sections

        if sections.count() <= 0:
            sections = Section.objects.filter(location=location)

        print sections

        for sec in sections:
            all_spaces = Space.objects.filter(section=sec, size=size, orientation=orientation).order_by('section','row')
            print all_spaces
            for space in all_spaces:
                booked_spaces = Space_Booking.objects.filter(space=space).filter(Q(status="B") | Q(status="R"))
                for bs in booked_spaces:
                    bs_start_date = bs.start_date
                    bs_end_date = bs.end_date

                    #check for overlap
                    if(start_date <= bs_end_date and bs_start_date <= end_date):
                        all_spaces = all_spaces.exclude(id=space.id)
                        break        


        if booking:
            advert = Advert.objects.get(id=ad_id)
            booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, end_date=end_date, quantity=quantity)
            if(all_spaces.count() >= int(quantity)):
                for i in range(0,int(quantity)):
                    Space_Booking.objects.create(space=all_spaces[i], advert=booked_ad, start_date=start_date, end_date=end_date,status="R")
           
        else:
            inventory = Inventory.objects.get(id=inv_id)
            #tomorrow -- check if inventory date is greater than today
            tomorrow = now().replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1)

            if end_date >= tomorrow:
                for i in range(0,int(quantity)):
                    Space_Booking.objects.create(space=all_spaces[i], start_date=start_date, end_date=end_date, status="B", inventory=inventory)

        num_spaces += all_spaces.count()

        return HttpResponse(num_spaces)
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Check Num Spaces: "+str(e))
        return HttpResponse(0)

def assign_space(section, orientation, size, booking=False):
    try:
        if booking:
            print "booking is True"
        else:
            #just assign space to this section
            print "booking is False"
            all_spaces = Space.objects.filter(section=sec, size=size, orientation=orientation).order_by('section','row')
            print all_spaces
            for space in all_spaces:
                booked_spaces = Space_Booking.objects.filter(space=space).filter(Q(status="B") | Q(status="R"))
                for bs in booked_spaces:
                    bs_start_date = bs.start_date
                    bs_end_date = bs.end_date

                    #check for overlap
                    if(start_date <= bs_end_date and bs_start_date <= end_date):
                        all_spaces = all_spaces.exclude(id=space.id)
                        break
            Space_Booking.objects.create(space=all_spaces[0], start_date=start_date, end_date=end_date, status="R")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Assign Space: "+str(e))
        return HttpResponse("Error: "+str(e))
    
#return number of spaces available for each AD and booking date
def check_num_spaces1(ad_id, location_id, size, orientation, quantity, start_date, end_date):
    """
        There are 3 options involved
        Opion 1: Check if space exists that can fit this AD size exactly on the specified dates
        Option 2: Check if space is big enough to fit an AD
        Option 3: Check if adjacent spaces can be combined to fit one AD
    """
    #get all spaces available in this location
    try:
        location = Location.objects.get(name=location_id)
        all_spaces = Space.objects.filter(section__location=location).order_by('section','row')
        #print all_spaces
        start_date = datetime.strptime(str(start_date), "%Y-%m-%d").date()
        end_date = datetime.strptime(str(end_date), "%Y-%m-%d").date()

        ad_width = 0
        ad_height = 0

        ad_size = str(size)+""+str(orientation[:1])

        ad_dimension = [s for s in SPACE_DIMENSION if s[0] == ad_size ]
        ad_width = float(ad_dimension[0][1])
        ad_height = float(ad_dimension[0][2])

        print ad_width
        print ad_height

        #for each space, check if the space has a booking that lies in between these dates, then exclude it in the final search list
        for space in all_spaces:
            booked_spaces = Space_Booking.objects.filter(space=space).filter(Q(status="B") | Q(status="R"))
            for bs in booked_spaces:
                bs_start_date = bs.start_date
                bs_end_date = bs.end_date

                #check for overlap
                if(start_date <= bs_end_date and bs_start_date <= end_date):
                    all_spaces = all_spaces.exclude(id=space.id)
                    break

        num_spaces = 0
        available_spaces = []

        if all_spaces.count() > 0:
            advert = Advert.objects.get(id=ad_id)
            fitting_spaces = all_spaces.filter(size=size, orientation=orientation)
            num_spaces = fitting_spaces.count()

            if fitting_spaces.count() >= int(quantity):

                booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, end_date=end_date, quantity=quantity)
                for i in range(0,int(quantity)):
                    Space_Booking.objects.create(space=fitting_spaces[i], advert=booked_ad, start_date=start_date, end_date=end_date, status="R")

                return num_spaces

            else: #Option 2                                
                for space in all_spaces:
                    space_size = str(space.size)+""+str(space.orientation[:1])                                      

                    space_dimension = [s for s in SPACE_DIMENSION if s[0] == space_size ]
                    space_width = float(space_dimension[0][1])
                    space_height = float(space_dimension[0][2])

                    width_ratio = int(space_width / ad_width)
                    height_ratio = int(space_height / ad_height)

                    if width_ratio == 1 and height_ratio == 1:
                        num_spaces += 1
                        available_spaces.append({'space':space, 'available':1,'status':'reserved'})

                    #ratio > 1, means this AD can fit in here -- check how many ADs can fit and increment """
                    elif width_ratio >= 1 and height_ratio >= 1:
                        num_spaces = num_spaces + (width_ratio * height_ratio)
                        available_spaces.append({'space':space, 'available':(width_ratio * height_ratio), 'status':'reserved'})

                    #stop looping when required quantity is reached
                    if int(num_spaces) >= int(quantity):
                        break

                #if quantity is reached, reserve AD space, and now create ADs and fix them in this position """
                if int(num_spaces) >= int(quantity):
                    print "entered option 2"
                    #loop through available spaces
                    booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, 
                        end_date=end_date, quantity=quantity)

                    num_bookings = int(quantity)
                    print available_spaces
                    print "===================="
                    for a_s in available_spaces:
                        free_spaces = int(a_s['available'])
                        
                        for i in range(0,int(free_spaces)):
                            if num_bookings >= 0:
                                Space_Booking.objects.create(space=a_s['space'],
                                    advert=booked_ad, start_date=start_date, end_date=end_date,status="R")                                

                            num_bookings = num_bookings - 1

                        if num_bookings <= 0:
                            break
                else:
                    """#Option 3 checking adjacent cells
                    for i in range(1,len(all_spaces)):
                        #check if this space is in available spaces - if its already there and is reserved, move on to the next space
                        required_height = 0
                        required_width = 0
                        
                        exists = [x for x in available_spaces if x['space'] == all_spaces[i]]

                        if len(exists) > 0:
                            #move on to the next cell, pass
                            print "this item exists has already been reserved"
                            pass
                        else:
                            #check what is lacking, is it width or height                            
                            c_space = all_spaces[i]
                            space_size = str(c_space.size)+""+str(c_space.orientation[:1])
                            current_space = [s for s in SPACE_DIMENSION if s[0] == space_size ]
                            space_width = float(current_space[0][1])
                            space_height = float(current_space[0][2])

                

                            width_ratio = int(space_width / ad_width)
                            height_ratio = int(space_height / ad_height)

                            right_space = Space.objects.filter(section=c_space.section, col=int(c_space.col)+1, row=int(c_space.row))
                            bottom_space = Space.objects.filter(section=c_space.section, col=c_space.col, row=int(c_space.row)+1)

                            right_space_width = 0
                            right_space_height = 0
                            bottom_space_width = 0
                            bottom_space_height = 0

                            if right_space.count() > 0:
                                right_space_size = str(right_space[0].size)+""+str(right_space[0].orientation[:1])
                                right_space_dimension = [s for s in SPACE_DIMENSION if s[0] == right_space_size ]

                                right_space_width = right_space_dimension[0][1]
                                right_space_height = right_space_dimension[0][2]

                            if bottom_space.count() > 0:
                                bottom_space_size = str(bottom_space[0].size)+""+str(bottom_space[0].orientation[:1])
                                bottom_space_dimension = [s for s in SPACE_DIMENSION if s[0] == bottom_space_size ]

                                bottom_space_width = bottom_space_dimension[0][1]
                                bottom_space_height = bottom_space_dimension[0][2]
                            
                            if width_ratio == 0 and height_ratio == 0:#expand both right and bottom
                                #check if there is space in the right or bottom first
                                if right_space.count() < 1 or bottom_space.count() < 1:
                                    continue

                                else:
                                    print "there is enough space to continue -- "
                                    required_height += float(bottom_space_height)
                                    required_width += float(right_space_width)

                                    if required_height <= ad_height and required_width <= ad_width:
                                        available_spaces.append({'space':c_space, 'available':1,'status':'reserved'})
                                        available_spaces.append({'space':bottom_space, 'available':1,'status':'reserved'})
                                        available_spaces.append({'space':right_space, 'available':1,'status':'reserved'})
                                        num_spaces += 1
                                                                        

                            elif width_ratio == 0 and height_ratio > 0:
                                if right_space.count() < 1:
                                    continue
                                else:
                                    required_width += float(right_space_width)

                                    if required_width <= ad_width:#check the attribute available:1
                                        available_spaces.append({'space':c_space, 'available':1,'status':'reserved'})
                                        available_spaces.append({'space':right_space, 'available':1,'status':'reserved'})
                                        num_spaces += 1
                                    else:
                                        print "continue searching"

                            elif width_ratio > 0 and height_ratio == 0:#only bottom space is required
                                if bottom_space.count() < 1:
                                    continue
                                else:
                                    required_height += float(bottom_space_height)

                                    if required_height <= ad_height:
                                        available_spaces.append({'space':c_space, 'available':1,'status':'reserved'})
                                        available_spaces.append({'space':bottom_space, 'available':1,'status':'reserved'})
                                        num_spaces += 1
                                    else:
                                        print "continue searching"

                    print "add it here"        
                    if int(num_spaces) >= int(quantity):
                        #loop through available spaces
                        booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, 
                            end_date=end_date, quantity=quantity)

                        print available_spaces

                        for a_s in available_spaces:
                            Space_Booking.objects.create(space=a_s['space'],
                                advert=booked_ad, start_date=start_date, end_date=end_date,status="R")
                                
                    #reset available spaces and do option 3
                    print "now working on option 3"""
                
                return num_spaces

        else:
            return num_spaces
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Check Num Spaces: "+str(e))
        return num_spaces


@login_required
def inventory_page(request):
    """
        For managing inventory by location
    """
    template_context = {}
    try:
        locations = Location.objects.all()
        current_location = locations[0]        
        if "location_id" in request.POST:
            location_id = request.POST['location_id']
            current_location = Location.objects.get(id=location_id)
            
        template_context['locations'] = locations
        template_context['current_location'] = current_location
        template_context['sections'] = Section.objects.filter(location=current_location).order_by("label")
        
        print locations[2]
        return render(request, "staff/inventory.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Inventory Page: "+str(e))
        return HttpResponse("error: "+str(e))

@login_required
def add_inventory_page(request, section_id):
    template_context = {}
    if request.user.is_staff:                
        template_context['upload_error'] = ""
        template_context['section'] = Section.objects.get(id=section_id)
        template_context['sizes'] = AD_SIZE
        template_context['orientation'] = AD_ORIENTATION
        template_context['categories'] = Category.objects.all()
        return render(request,"staff/add_inventory.html",template_context)            
    else:        
        return redirect("/staff/logout/")

@login_required
def view_inventory_page(request, section_id):
    template_context = {}
    if request.user.is_staff:      
        section = Section.objects.get(id=section_id)
        locations = Location.objects.all()          
        current_location = section.location
        template_context['locations'] = locations
        template_context['current_location'] = current_location
        template_context['section'] = section
        template_context['inventory'] = Inventory.objects.filter(section=section)

        return render(request,"staff/view_inventory.html",template_context)            
    else:        
        return redirect("/staff/logout/")

@login_required
def create_inventory(request):
    try:
        receipt_no = request.POST['receipt_no']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        phone_number = request.POST['mobile_number']
        email_address = request.POST['email_address']
        ad_category = request.POST['ad_category']
        ad_size = request.POST['ad_size']
        ad_orientation = request.POST['ad_orientation']
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
        section_id = request.POST['section_id_2']

        category = Category.objects.get(id=ad_category)
        section = Section.objects.get(id=section_id)

        inventory = Inventory.objects.create(receipt_no=receipt_no, first_name=first_name, last_name=last_name, phone_number=phone_number, email_address=email_address, ad_size=ad_size, ad_orientation=ad_orientation, ad_category=category, start_date=start_date, end_date=end_date, section=section)

        check_num_spaces(section.location.id, ad_size, ad_orientation, 1, start_date, end_date, 1, False, None, inventory.id)

        return HttpResponse("success")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Create Inventory: "+str(e))
        return HttpResponse(e)


@login_required
def upload_csv_inventory(request):
    template_context = {}
    try:                
        section_id = request.POST['section_id_1']
        section = Section.objects.get(id=section_id)
        file = request.FILES['csv_file']
        data = [row for row in csv.reader(file.read().splitlines())] 
        for item in data:
            if item[0] != "Receipt No":
                category = Category.objects.get(name=item[7])
                inventory = Inventory.objects.create(receipt_no=item[0], first_name=item[1], last_name=item[2], phone_number=item[3], email_address=item[4], ad_size=item[5], ad_orientation=item[6], ad_category=category, start_date=item[8], end_date=item[9], section=section)

                check_num_spaces(section.location.id, item[5], item[6], 1, item[8], item[9], 1, False, None, inventory.id)

        return render(request, "staff/add_inventory.html", {})
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Inventory - Upload CSV: "+str(e))        
        return HttpResponse(e)

@login_required
def search_inventory(request):
    try:
        customer_name = request.POST['customer_name']
        receipt_no = request.POST['receipt_no']
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']

        searchFound = False
        search_results = Inventory.objects.all()
        if len(customer_name) > 0:
            search_results = search_results.filter(Q(first_name__contains=customer_name) | Q(first_name__contains=customer_name))
            searchFound = True
        if len(receipt_no) > 0:
            search_results =  search_results.filter(receipt_no=receipt_no) 
            searchFound = True
        if len(start_date) > 0:
            search_results =  search_results.filter(start_date=start_date) 
            searchFound = True
        if len(end_date) > 0:
            search_results =  search_results.filter(start_date=start_date) 
            searchFound = True

        if searchFound:
            template_context = {}
            template_context['search_results'] = search_results
            return render(request, "staff/search_inventory.html", template_context)
        else:
            return render(request, "staff/inventory.html", {'search_error':'No record found.'})
            
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Search Inventory: "+str(e))
        return render(request, "staff/inventory.html", {})

@login_required
def job_schedule_page(request):
    template_context = {}
    try:
        #template_context = 
        return render(request, "staff/job_schedule.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Jobs Page: "+str(e))
        return HttpResponse(e)

@login_required
def print_jobs_page(request):
    """
       Select all ADs due for printing today by location -- start_date ==> tomorrow and status is ready
    """
    template_context = {}    
    locations = Location.objects.all()
    paper_types_list = []

    current_loc = ""
    booked_ads = []
    
    try:
        today = now().replace(hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today + timedelta(days=1)
        start_date = tomorrow
        end_date = tomorrow+timedelta(days=1)

        if "location" in request.POST:
            current_loc = Location.objects.get(id=request.POST['location'])
            date = request.POST["date"]
            if len(date) > 0:
                start_date = datetime.strptime(str(date), "%Y-%m-%d")
                end_date = start_date + timedelta(days=1)
        else:
            current_loc = locations[0]

        template_context['locations'] = locations        
        template_context['current_loc'] = current_loc
        
        
        booked_ads = print_jobs(start_date, end_date, current_loc)

        ads_ret_dict = {
            'children':[]
        }

        for size in AD_SIZE:
            #check paper type and group them
            for p_type in AD_PAPER_TYPE:
                #check whether such ads exist
                p_type_ads = booked_ads.filter(advert__size=size[0], advert__paper_type=p_type[0])
                                
                if p_type_ads.count() > 0:    
                    paper_ret_dict = {
                        'type':{},
                        'number':{},
                        'size':{},
                        'ads':[]
                    }
                    paper_ret_dict['type'] = p_type[0]
                    paper_ret_dict['size'] = size[0]
                    paper_ret_dict['number'] = p_type_ads.count()
                    paper_ret_dict['ads'] = p_type_ads

                    ads_ret_dict['children'].append(paper_ret_dict)
            
        template_context['ads_by_size'] = ads_ret_dict
        template_context['count'] = booked_ads.count()
        template_context['name'] = locations[0]
        template_context['date'] = start_date.date()
        template_context['day'] = start_date.date().day
        template_context['month'] = start_date.date().month
        template_context['year'] = start_date.date().year
        template_context['booked_ads'] = booked_ads

        threshold = (end_date.replace(tzinfo=None) - now().replace(tzinfo=None)).days
        
        template_context['threshold'] = threshold
        template_context['responsible'] = str(request.user.first_name) + " " + str(request.user.last_name)

        print template_context

        return render(request, "staff/print_jobs.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Print Jobs: "+str(e))
        return HttpResponse(e)

@login_required
def delivery_jobs_page(request):
    """
       Select all ADs due for delivery today by location -- start_date ==> tomorrow and status is ready
    """
    template_context = {}    
    locations = Location.objects.all()
    paper_types_list = []

    current_loc = ""
    booked_ads = []
    
    try:
        today = now().replace(hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today + timedelta(days=1)
        start_date = tomorrow
        end_date = tomorrow+timedelta(days=1)

        if "location" in request.POST:
            current_loc = Location.objects.get(id=request.POST['location'])
            date = request.POST["date"]
            """if date != "" or date is not "":"""
            if len(date) > 0:
                start_date = datetime.strptime(str(date), "%Y-%m-%d")
                end_date = start_date + timedelta(days=1)
        else:
            current_loc = locations[0]

        template_context['locations'] = locations        
        template_context['current_loc'] = current_loc
        
        
        booked_ads = delivery_jobs(start_date, end_date, current_loc)

        ads_ret_dict = {
            'children':[]
        }

        for size in AD_SIZE:
            for p_type in AD_PAPER_TYPE:
                p_type_ads = booked_ads.filter(advert__size=size[0], advert__paper_type=p_type[0])
                                
                if p_type_ads.count() > 0:    
                    paper_ret_dict = {
                        'type':{},
                        'number':{},
                        'size':{},
                        'ads':[]
                    }
                    paper_ret_dict['type'] = p_type[0]
                    paper_ret_dict['size'] = size[0]
                    paper_ret_dict['number'] = p_type_ads.count()
                    paper_ret_dict['ads'] = p_type_ads

                    ads_ret_dict['children'].append(paper_ret_dict)
            
        template_context['ads_by_size'] = ads_ret_dict
        template_context['count'] = booked_ads.count()
        template_context['name'] = locations[0]
        template_context['date'] = start_date.date()
        template_context['day'] = start_date.date().day
        template_context['month'] = start_date.date().month
        template_context['year'] = start_date.date().year
        template_context['booked_ads'] = booked_ads

        threshold = (end_date.replace(tzinfo=None) - now().replace(tzinfo=None)).days
        
        template_context['threshold'] = threshold#int(threshold.total_seconds() / 3600000)
        template_context['responsible'] = str(request.user.first_name) + " " + str(request.user.last_name)

        print template_context

        return render(request, "staff/delivery_jobs.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Delivery Jobs: "+str(e))
        return HttpResponse(e)

@login_required
def posting_jobs_page(request):
    """
       Select all ADs due for posted today by location -- start_date ==> tomorrow and status is ready
    """
    template_context = {}    
    locations = Location.objects.all()
    paper_types_list = []

    current_loc = ""
    booked_ads = []
    
    try:
        today = now().replace(hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today + timedelta(days=1)
        start_date = tomorrow
        end_date = tomorrow+timedelta(days=1)

        if "location" in request.POST:
            current_loc = Location.objects.get(id=request.POST['location'])
            date = request.POST["date"]
            """if date != "" or date is not "":"""
            if len(date) > 0:
                start_date = datetime.strptime(str(date), "%Y-%m-%d")
                end_date = start_date + timedelta(days=1)
        else:
            current_loc = locations[0]

        template_context['locations'] = locations        
        template_context['current_loc'] = current_loc
        
        
        booked_ads = posting_jobs(start_date, end_date, current_loc)

        ads_ret_dict = {
            'children':[]
        }

        for size in AD_SIZE:
            for p_type in AD_PAPER_TYPE:
                p_type_ads = booked_ads.filter(advert__size=size[0], advert__paper_type=p_type[0])
                                
                if p_type_ads.count() > 0:    
                    paper_ret_dict = {
                        'type':{},
                        'number':{},
                        'size':{},
                        'ads':[]
                    }
                    paper_ret_dict['type'] = p_type[0]
                    paper_ret_dict['size'] = size[0]
                    paper_ret_dict['number'] = p_type_ads.count()
                    paper_ret_dict['ads'] = p_type_ads

                    ads_ret_dict['children'].append(paper_ret_dict)
            
        template_context['ads_by_size'] = ads_ret_dict
        template_context['count'] = booked_ads.count()
        template_context['name'] = locations[0]
        template_context['date'] = start_date.date()
        template_context['day'] = start_date.date().day
        template_context['month'] = start_date.date().month
        template_context['year'] = start_date.date().year
        template_context['booked_ads'] = booked_ads

        threshold = (end_date.replace(tzinfo=None) - now().replace(tzinfo=None)).days
        
        template_context['threshold'] = threshold
        template_context['responsible'] = str(request.user.first_name) + " " + str(request.user.last_name)

        print template_context

        return render(request, "staff/posting_jobs.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Posting Jobs: "+str(e))
        return HttpResponse(e)

@login_required
def removal_jobs_page(request):
    """
       Select all ADs due for printing today by location -- start_date ==> tomorrow and status is ready
    """
    template_context = {}    
    locations = Location.objects.all()
    paper_types_list = []

    current_loc = ""
    booked_ads = []
    
    try:
        today = now().replace(hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today + timedelta(days=1)
        start_date = tomorrow
        end_date = tomorrow+timedelta(days=1)

        if "location" in request.POST:
            current_loc = Location.objects.get(id=request.POST['location'])
            date = request.POST["date"]
            """if date != "" or date is not "":"""
            if len(date) > 0:
                start_date = datetime.strptime(str(date), "%Y-%m-%d")
                end_date = start_date + timedelta(days=1)
        else:
            current_loc = locations[0]

        template_context['locations'] = locations        
        template_context['current_loc'] = current_loc
        
        
        booked_ads = removal_jobs(start_date, end_date, current_loc)

        ads_ret_dict = {
            'children':[]
        }

        for size in AD_SIZE:
            for p_type in AD_PAPER_TYPE:
                p_type_ads = booked_ads.filter(advert__size=size[0], advert__paper_type=p_type[0])
                                
                if p_type_ads.count() > 0:    
                    paper_ret_dict = {
                        'type':{},
                        'number':{},
                        'size':{},
                        'ads':[]
                    }
                    paper_ret_dict['type'] = p_type[0]
                    paper_ret_dict['size'] = size[0]
                    paper_ret_dict['number'] = p_type_ads.count()
                    paper_ret_dict['ads'] = p_type_ads

                    ads_ret_dict['children'].append(paper_ret_dict)
            
        template_context['ads_by_size'] = ads_ret_dict
        template_context['count'] = booked_ads.count()
        template_context['name'] = locations[0]
        template_context['date'] = start_date.date()
        template_context['day'] = start_date.date().day
        template_context['month'] = start_date.date().month
        template_context['year'] = start_date.date().year
        template_context['booked_ads'] = booked_ads

        threshold = (end_date.replace(tzinfo=None) - now().replace(tzinfo=None)).days
        
        template_context['threshold'] = threshold
        template_context['responsible'] = str(request.user.first_name) + " " + str(request.user.last_name)

        print template_context

        return render(request, "staff/removal_jobs.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Removal Jobs: "+str(e))
        return HttpResponse(e)


def print_jobs(start_date=None, end_date=None, location=None):
    try:
        return Booked_Advert.objects.filter(location=location, start_date__gte=start_date, start_date__lt=end_date, status="Ready")
    except Exception as e:
        return HttpResponse(e)

def delivery_jobs(start_date=None, end_date=None, location=None):
    try:
        return Booked_Advert.objects.filter(location=location, start_date__gte=start_date, start_date__lte=end_date, status="Printed")
    except Exception as e:
        return HttpResponse(e)

def posting_jobs(start_date=None, end_date=None, location=None):
    try:
        return Booked_Advert.objects.filter(location=location, start_date__gte=start_date, start_date__lte=end_date, status="Awaiting Delivery")
    except Exception as e:
        return HttpResponse(e)

def removal_jobs(start_date=None, end_date=None, location=None):
    try:
        return Booked_Advert.objects.filter(location=location, start_date__gte=start_date, start_date__lte=end_date, status="Posted")
    except Exception as e:
        return HttpResponse(e)
