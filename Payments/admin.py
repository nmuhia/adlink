from django.contrib import admin
from models import Payment

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('amount','user','advert',)
    readonly_fields = ('timestamp',)

admin.site.register(Payment, PaymentAdmin)
