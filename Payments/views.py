from datetime import date, timedelta, datetime

from django.http import HttpResponse
from django.utils import timezone
from django.utils.timezone import now,utc

from Payments.models import Payment

def get_payments(user, start_date=None, end_date=None):
    """
        Returns chart data for selected dates
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7

    ret_dict = {
        'total': [],
        'dates':[],
        'count':[]
    }

    all_payments = Payment.objects.all()

    if start_date is None:
        end_date = now().replace(hour=0, minute=0, second=0, microsecond=0)
        start_date = end_date - timedelta(days=8)
        #all_payments = all_payments.filter(timestamp__gte=start_date, timestamp__lte=end_date)

    else: 
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
        start_date = timezone.make_aware(start_date,timezone=utc)
        end_date = timezone.make_aware(end_date,timezone=utc)
        ACTIVITY_GRAPH_NUM_DAYS = (end_date - start_date).days + 1

    all_payments = all_payments.filter(timestamp__gte=start_date, timestamp__lte=end_date+timedelta(days=1))
    

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        total = 0
        count = 0
        next_day = end_date - timedelta(days=i) 
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        
        payments = Payment.objects.filter(timestamp__gte=next_day, timestamp__lte=next_day + timedelta(days=1))
        if len(payments) > 0:
            count = payments.count()

        for pay in payments:
            total += float(pay.amount)
        ret_dict['total'].append(total)
        ret_dict['count'].append(count)

    return [ret_dict, all_payments]
