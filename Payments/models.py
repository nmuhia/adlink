from django.db import models
from django.contrib.auth.models import User
from Adverts.models import Advert

# Create your models here.

PAYMENT_METHOD = (
    ('M-PESA', 'M-PESA'),
    ('Cash', 'Cash'),
)

class Payment(models.Model):
    raw_message = models.CharField(max_length=1000, verbose_name="Raw Message", blank=True, null=True)
    amount = models.DecimalField(max_digits=14, decimal_places=2,verbose_name="Payment Amount")
    user = models.ForeignKey(User, blank=False, null=False,default=None)
    timestamp = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    reference = models.CharField(max_length=50, blank=True, null=True, unique=True)
    advert = models.ForeignKey(Advert, blank=False, null=False)
    method = models.CharField(max_length=10, choices=PAYMENT_METHOD,default="M-PESA")

    def __unicode__(self):
        return str(self.advert)+" : "+str(self.user) + " : " + str(self.amount) + " : " + str(self.timestamp)
    
    class Meta:
        verbose_name = "Payment"
        ordering = ['-timestamp']
