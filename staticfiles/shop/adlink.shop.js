var cartName = "adlink-shop";
var storage = sessionStorage;
var total = parseFloat(0).toFixed(2);;
var gloss_price = parseFloat(0).toFixed(2);;
var normal_price = parseFloat(0).toFixed(2);;
var lamination_price = parseFloat(0).toFixed(2);;
var selected_ad_size = "A1";
var selected_ad_orientation = "Potrait";

var daily_rates = [];
var weekly_rates = [];
var monthly_rates = [];
var yearly_rates = [];
var available_spaces = [];

total_print_price = gloss_price + normal_price + lamination_price;

total_price = 0.00;
total_copies = 0;
grand_price = 0.00;

function check_discounts_that_apply(){
    //check by location
    //get by location
    //check by num of location
    //get number of locations
    //check by duration
    //get duration

}

function calculateTotalPrintPrice(normal_price, gloss_price, lamination_price){    
    total_print_price = parseFloat(normal_price) + parseFloat(gloss_price) + parseFloat(lamination_price);
    return total_print_price.toFixed(2);
}

function updatePrintPrice(normal_price, gloss_price, lamination_price, ad_size){
    //normal pricing
    if(normal_price == 0){
        $(".normal_price_tr").css("display","none");        
    }
    else{
        $(".normal_price_tr").css("display","block");
        $(".gloss_price_tr").css("display","none");
        $(".normal_price_title").text("Normal ("+ad_size+")");
        $(".summary_normal_price").text(normal_price);
    }

    //gloss pricing
    if(gloss_price == 0){
        $(".gloss_price_tr").css("display","none");        
    }
    else{
        $(".gloss_price_tr").css("display","block");
        $(".normal_price_tr").css("display","none");        
        $(".gloss_price_title").text("Gloss ("+ad_size+")");
        $(".summary_gloss_price").text(gloss_price);
    }

    //lamination pricing
    if(lamination_price == 0){
        $(".lamination_price_title").text("Lamination");
        $(".summary_lamination_price").text(lamination_price);
    }
    else{
        $(".lamination_price_title").text("Lamination");
        $(".summary_lamination_price").text(lamination_price);
    }
        
    $(".total_print_price").text(total_print_price);
}

/*function total_location_rate(days, daily_rate){
    return (parseFloat(daily_rate) * parseFloat(days)).toFixed(2);
}*/

function get_price_per_day(size, location)
{
        price = 0;                
        //loop through daily rates 
        for(i=0;i<daily_rates.length;i++){    
            if(daily_rates[i]['location'] == location){
                if(size == "A1"){
                    price = daily_rates[i]['a1'];
                }
                else if(size == "A2"){
                    price = daily_rates[i]['a2'];
                }
                else if(size == "A3"){
                    price = daily_rates[i]['a3'];
                }
                else if(size == "A4"){
                    price = daily_rates[i]['a4'];
                }
                else if(size == "A5"){
                    price = daily_rates[i]['a5'];
                }
                break;
            }            
        }  
        return price;
}
function get_price_per_week(size, location)
{        
        price = 0;                
        //loop through weekly rates 
        for(i=0;i<weekly_rates.length;i++){    
            if(weekly_rates[i]['location'] == location){
                if(size == "A1"){
                    price = weekly_rates[i]['a1'];
                }
                else if(size == "A2"){
                    price = weekly_rates[i]['a2'];
                }
                else if(size == "A3"){
                    price = weekly_rates[i]['a3'];
                }
                else if(size == "A4"){
                    price = weekly_rates[i]['a4'];
                }
                else if(size == "A5"){
                    price = weekly_rates[i]['a5'];
                }
                break;
            }            
        }  
        return price;
}

function get_price_per_month(size, location)
{        
        price = 0;                
        //loop through monthly rates 
        for(i=0;i<monthly_rates.length;i++){    
            if(monthly_rates[i]['location'] == location){
                if(size == "A1"){
                    price = monthly_rates[i]['a1'];
                }
                else if(size == "A2"){
                    price = monthly_rates[i]['a2'];
                }
                else if(size == "A3"){
                    price = monthly_rates[i]['a3'];
                }
                else if(size == "A4"){
                    price = monthly_rates[i]['a4'];
                }
                else if(size == "A5"){
                    price = monthly_rates[i]['a5'];
                }
                break;
            }            
        }  
        return price;
}
function get_price_per_year(size, location)
{        
        price = 0;                
        //loop through yearly rates 
        for(i=0;i<yearly_rates.length;i++){    
            if(yearly_rates[i]['location'] == location){
                if(size == "A1"){
                    price = yearly_rates[i]['a1'];
                }
                else if(size == "A2"){
                    price = yearly_rates[i]['a2'];
                }
                else if(size == "A3"){
                    price = yearly_rates[i]['a3'];
                }
                else if(size == "A4"){
                    price = yearly_rates[i]['a4'];
                }
                else if(size == "A5"){
                    price = yearly_rates[i]['a5'];
                }
                break;
            }            
        }  
        return price;
}


//update total number of copies
function update_total_copies(){
    //loop through all locations
    total_copies = 0;
    $('.ad_location_date_table tr').find('.quantity').each(function(){
        total_copies = total_copies + parseInt($(this).val());
    });    
    print_price = total_print_price * total_copies;
    $(".show_print_total").text(print_price.toFixed(2));
    $(".show_print_copies").text(total_copies);
    return total_copies;

}

//update total price
function update_total_price(){
    total_price = 0.00;

    $('.ad_location_date_table tr').find('.price').each(function(){
        if($(this).text() == ""){
            $(this).text("0");
        }
        total_price = total_price + parseFloat($(this).text());
    });
    $(".show_total").text(total_price.toFixed(2));
    return total_price;
}

//update grand price
function update_grand_price(){
    total_price = 0.00;
    total_copies = 0;
    total_price = update_total_price();
    total_copies = update_total_copies();

    print_price = total_print_price * total_copies;
    grand_price = total_price + print_price;

    $(".show_grand_total").text(grand_price.toFixed(2));
}

function createCart(){				
    if(this.storage.getItem(this.cartName) == null || this.storage.getItem(this.cartName) == ""){
        var cart = {};
        cart.items = [];
        this.storage.setItem( this.cartName, this._toJSONString(cart));	
    }
}

function checkIfExists(values){		
    var cart = this.storage.getItem( this.cartName );
    var cartObject = this._toJSONObject( cart );
    var items=cartObject.items;

    for(var i=0;i<items.length;i++){
        if(values.recipe_id==items[i].recipe_id){				
            return 1;
            break;
        }
    }
}

function addToCart(values){
    //console.log("VALUES: "+values);
    var exists=checkIfExists(values);
    if(exists==1){			
    }
    else{
        var cart = this.storage.getItem( this.cartName );
        var cartObject = this._toJSONObject( cart );			
        var items = cartObject.items;
        items.push( values);
        //for the values that are being added to cart, also there is an area where the 
        for(var i=0;i<items.length;i++){
            //console.log("ITEM"+items[i]);
        }
        this.storage.setItem( this.cartName, this._toJSONString( cartObject ) );					
    }
    $("#location_date_summary").val(items);
    /* display a cart whenever something is added*/
    //displayCart();

}

function displayCart(){
    var subTotal=0;
    if(this.storage.length){
        var cart = this.storage.getItem( this.cartName );
        var cartObject = this._toJSONObject( cart );	
        var items = cartObject.items;	
        
        for (var i =0; i < items.length; i++){  
            var category_name = items[i].category;
            var location = items[i].locations;
        }
    }
}



function emptyCart(){	
//    console.log("This cart is being emptied");
    this.storage.clear();
}

//private methods
function _toJSONString(obj){
    var str=JSON.stringify(obj);
    return str;
}

function _toJSONObject(str){
    var obj=JSON.parse(str);
    return obj;
}
