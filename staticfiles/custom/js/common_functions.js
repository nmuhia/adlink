function validateEmail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        return false;
    }
    else{
        return true;
    }
}

function validatePhone(phoneNo){
    //check if the number is numeric
    String.prototype.isNumber = function(){
        return /^\d+$/.test(this);
    }

    if(phoneNo.charAt(0) == "+"){
        new_phoneNo = phoneNo.substring(1);
        if(new_phoneNo.isNumber()){    
            return true;
        }
        else{
            return false;
        }
    }
    else{
        if(phoneNo.isNumber()){
            return true;
        }
        else{
            return false;
        }
    }
}

function dashed_formatted_date(date){  
    var month = (date.getUTCMonth()+1).toString();
    var day = date.getUTCDate().toString();
    var year = date.getUTCFullYear();    
    
    day = day.length < 2 ? '0' + day : day;
    month = month.length < 2 ? '0' + month : month;
  
    var formatted_date = year+"-"+month+"-"+day;       
    return formatted_date;
}

function fromDays(num){
    return 86400000*2*parseInt(num);
}
function fromWeeks(num){
    //to microseconds
    return 86400000*8*parseInt(num);
}
function fromMonths(num){    
    return 86400000*31*parseInt(num);
}

