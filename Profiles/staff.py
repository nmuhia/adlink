import json
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password, check_password

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.context_processors import csrf
from django.core import serializers
from django.core.files import File

from django.http import HttpResponse, Http404
from django.core.management import call_command

import datetime
from datetime import date, timedelta, datetime

from django.utils import timezone
from django.utils.timezone import now,utc
from django.db.models import Q,Count

from models import User_Profile

from Locations.models import Location, DURATION, Space_Booking
from Locations.views import show_daily_rates, show_weekly_rates, show_monthly_rates, show_yearly_rates

from Adverts.models import Advert, Booked_Advert, Category, Pricing, Error_Log, AD_SIZE, AD_ORIENTATION, AD_PAPER_TYPE, AD_PAPER_EXTRA_OPTIONS
from Adverts.views import get_categories_list, show_ads, get_pricing_information, get_ad_details, get_booked_ad_details
from Adverts.charts import get_booked_ads_data_chart

from Payments.models import Payment
from Payments.views import get_payments

def login_form(request):
    return render(request,"staff/login.html",{})

def login_page(request):
    """
    Handles logging in of an administrative user.
    """
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')

    if username == "" or password == "":
        return render(request, "staff/login.html",
                              {'login_error': 'Please fill in your username and password'})
    else:

        if request.user.is_authenticated():
            #return redirect(request.GET.get('next', '/staff/dashboard/'))
            return redirect("/staff/dashboard/")

        if request.method == "POST":
            user = authenticate(username=username,
                                password=password)
            if user is not None:
                if user.is_active:
                    login(request,user)
                    #return redirect(request.GET.get('next', '/staff/dashboard/'))
                    return redirect("/staff/dashboard/")
                else:
                    return render(request, "staff/login.html",
                                  {'login_error': 'Your account has been disabled. Please contact your administrator.'})
            else:
                return render(request, "staff/login.html",
                              {'login_error': 'You have entered the wrong username or password. Please try again.'})

    return render(request, "staff/login.html", {})

def logout_page(request):
    """
    Handles logging out of an administrative user.
    """
    try:
        logout(request)
        return redirect("/staff/")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))

def forgot_password_page(request):
    """
        Handles password resets
    """
    return redirect("/staff/")

@login_required
def dashboard_page(request):
    """
        Dashboard/Gateway for for staff/superuser functions
    """
    try:
        today = now().replace(hour=0, minute=0, second=0, microsecond=0)
        tomorrow = today + timedelta(days=1)
        ads_list = []
        template_context = {}
        template_context['booked_ads'] = get_booked_ads_data_chart(None, None)
        template_context['revenues'] = get_payments(None, None)
        template_context['my_tasks'] = 0
        template_context['users_logged'] = 0
        template_context['clients_logged'] = 0
        template_context['bookings'] = Advert.objects.filter(timestamp__gte=today, timestamp__lt=tomorrow).count()
        template_context['page_views'] = 0
        template_context['online'] = 0
        template_context['offline'] = Booked_Advert.objects.all().count()
        template_context['abadoned_carts_percentage'] = 0
        template_context['abadoned_carts'] = 0
        template_context['logins_percentage'] = 0
        template_context['logins'] = 0
        template_context['orders_percentage'] = 0
        template_context['orders'] = 0        
        template_context['booked_ads'] = get_booked_ads_data_chart(None, None)
        
        locations = Location.objects.all().order_by("name")

        for loc in locations:
            ad_info = {}
            ad_info['loc'] = loc.name
            ad_info['ads'] = Booked_Advert.objects.filter(location=loc).count()
            ad_info['last_ads'] = Booked_Advert.objects.filter(location=loc, advert__timestamp__gte=now() - timedelta(days=7)).count()
            ads_list.append(ad_info)
        template_context['ads_count'] = ads_list
        return render(request, "staff/dashboard.html", template_context)

    except Exception as e:
        return HttpResponse(e)

@login_required
def adverts_page(request):
    """
        Manage Adverts page
    """
    template_context = {}
    #get adverts booked in the last 4 days
    template_context['ads'] = Advert.objects.filter(timestamp__gte=(timezone.now() - timedelta(days=3)))
    template_context['categories'] = Category.objects.all()
    return render(request, "staff/adverts.html", template_context)

@login_required
def filter_ads(request):    
    ads_list = []
    try:
        category_id = request.POST['category']
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']

        ads = Advert.objects.all()   

        if category_id != "0":
            category = Category.objects.get(id=category_id)
            ads = ads.filter(category=category)
        if start_date != "":
            ads = ads.filter(timestamp__gte=start_date, timestamp__lte=end_date)

        for ad in ads:
            ad_info = {}
            ad_info['id'] = ad.id
            ad_info['ad_id'] = "AD - "+str('%03d' % ad.id)
            ad_info['size'] = ad.size
            ad_info['user'] = str(ad.user.first_name) + " "+str(ad.user.last_name)
            ad_info['user_id'] = str(ad.user.id)
            ad_info['orientation'] = ad.orientation
            ad_info['category'] = ad.category.name

            ads_list.append(ad_info)

        return HttpResponse(json.dumps(ads_list))
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Filter ADs: "+str(e))
        return HttpResponse(json.dumps(ads_list))


@login_required
def reset_filter_ads(request):
    ads_list = []
    try:
        ads = Advert.objects.filter(timestamp__gte=(timezone.now() - timedelta(days=3)))
        for ad in ads:
            ad_info = {}
            ad_info['id'] = ad.id
            ad_info['ad_id'] = "AD - "+str('%03d' % ad.id)
            ad_info['size'] = ad.size
            ad_info['user'] = str(ad.user.first_name) + " "+str(ad.user.last_name)
            ad_info['user_id'] = str(ad.user.id)
            ad_info['orientation'] = ad.orientation
            ad_info['category'] = ad.category.name

            ads_list.append(ad_info)

        return HttpResponse(json.dumps(ads_list))
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Filter ADs: "+str(e))
        return HttpResponse(json.dumps(ads_list))

@login_required
def ad_info(request, ad_id):
    """
        Return AD details page
    """
    template_context = {}
    booked_locs = []
    try:
        advert = Advert.objects.get(id=ad_id)
        booked_ads = Booked_Advert.objects.filter(advert=advert)
        #template_context['advert'] = get_ad_details(ad_id)
        template_context['advert'] = advert
        template_context['payment'] = Payment.objects.get(advert=advert)
        template_context['booked_ads'] = booked_ads
        for ad in booked_ads:
            if ad.location in booked_locs:
                pass
            else:
                booked_locs.append(ad.location)
        template_context['booked_locs'] = booked_locs
        template_context['booked_spaces'] = Space_Booking.objects.filter(advert__advert=advert)
        return render(request, "staff/ad_details.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Detail: "+str(e))
        return HttpResponse("Error showing your AD")


@login_required
def booked_ad_details(request, booking_ad_id):
    """
        Return AD details page
    """
    template_context = {}
    try:
        template_context.update(get_booked_ad_details(booking_ad_id))
        return render(request, "staff/booked_ad_details.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Booked AD Detail: "+str(e))
        return HttpResponse("Error showing your AD")

@login_required
def customers_page(request):
    """
        Manage Clients page
    """
    template_context = {}
    if request.user.is_superuser:
        template_context['superuser'] = True

    clients_list = []
    for user in User.objects.filter(Q(is_superuser=False), Q(is_staff=False)):  
        user_profile = User_Profile.objects.filter(user=user)
        if len(user_profile) == 0:
            #create profile
            user_prof = User_Profile.objects.create(user=user)
        else:
            user_prof = User_Profile.objects.get(user=user)
        client_info = {}
        client_info['client'] = user
        client_info['phone_number'] = user_prof.phone_number

        clients_list.append(client_info)
    template_context['clients_list'] = clients_list
    
    return render(request, "staff/customers.html", template_context)

@login_required
def add_customer_form(request):
    try:
        return render(request, "staff/add_customer.html", {})
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Customer: "+str(e))

@login_required
def add_customer(request):
    is_active = True

    is_active_value = request.POST["is_active"]

    if is_active_value == "false":
        is_active = False
           
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    email_address = request.POST['email_address']
    mobile_number = request.POST['mobile_number']    

    check_email = User.objects.filter(email=email_address)
    check_email = check_email.exclude(email="")
    check_mobile = User_Profile.objects.filter(phone_number=mobile_number)
    check_mobile = check_mobile.exclude(phone_number="")

    user = ""
    user_profile = ""


    if len(check_email) > 0:
        return HttpResponse("exists")

    elif len(check_mobile) > 0:            
        return HttpResponse("exists")

    else:
        try:
            if email_address != "":
                user = User.objects.create(first_name=first_name, last_name=last_name, email=email_address,username=email_address, is_active=is_active)

            else:            
                user = User.objects.create(first_name=first_name, last_name=last_name, email=email_address, username=mobile_number, is_active=is_active)

            profile = User_Profile.objects.create(user=user, phone_number=mobile_number) 
            profile.save()

            return HttpResponse("success")
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Customer: "+str(e))
            return HttpResponse("error")


@login_required
def personnel_page(request):
    """
        Manage Clients page
    """
    template_context = {}

    personnel_list = []
    for user in User.objects.filter(is_staff=True):  
        user_profile = User_Profile.objects.filter(user=user)
        if len(user_profile) == 0:
            #create profile
            user_prof = User_Profile.objects.create(user=user)
        else:
            user_prof = User_Profile.objects.get(user=user)
        personnel_info = {}
        personnel_info['personnel'] = user
        personnel_info['phone_number'] = user_prof.phone_number

        personnel_list.append(personnel_info)
    template_context['personnel_list'] = personnel_list
    
    return render(request, "staff/personnel.html", template_context)

@login_required
def add_personnel_form(request):
    try:
        return render(request, "staff/add_personnel.html", {})
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Customer: "+str(e))

@login_required
def add_personnel(request):
    is_active = True
    is_superuser= False

    is_active_value = request.POST["is_active"]
    is_superuser_value = request.POST["is_superuser"]

    if is_active_value == "false":
        is_active = False

    if is_superuser_value == "true":
        is_superuser = True
           
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    email_address = request.POST['email_address']
    mobile_number = request.POST['mobile_number']    

    check_email = User.objects.filter(email=email_address)
    check_email = check_email.exclude(email="")
    check_mobile = User_Profile.objects.filter(phone_number=mobile_number)
    check_mobile = check_mobile.exclude(phone_number="")

    user = ""
    user_profile = ""


    if len(check_email) > 0:
        return HttpResponse("exists")

    elif len(check_mobile) > 0:            
        return HttpResponse("exists")

    else:
        try:
            if email_address != "":
                user = User.objects.create(first_name=first_name, last_name=last_name, email=email_address,username=email_address, is_active=is_active, is_staff=True, is_superuser=is_superuser)

            else:            
                user = User.objects.create(first_name=first_name, last_name=last_name, email=email_address, username=mobile_number, is_active=is_active, is_staff=True, is_superuser=is_superuser)

            profile = User_Profile.objects.create(user=user, phone_number=mobile_number) 
            profile.save()

            return HttpResponse("success")
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Customer: "+str(e))
            return HttpResponse("error")



@login_required
def staff_page(request):
    """
        Manage Staff page
    """
    template_context = {}
    staff_list = []
    for user in User.objects.filter(is_staff=True):  
        user_profile = User_Profile.objects.filter(user=user)
        if len(user_profile) == 0:
            #create profile
            user_prof = User_Profile.objects.create(user=user)
        else:
            user_prof = User_Profile.objects.get(user=user)
        staff_info = {}
        staff_info['staff'] = user
        staff_info['phone_number'] = user_prof.phone_number

        staff_list.append(staff_info)
    template_context['staff_list'] = staff_list
    return render(request, "staff/staff.html", template_context)


@login_required
def add_user(request):
    user_type = request.POST["type"]
    is_superuser = False
    is_staff = False
    is_active = True

    is_active_value = request.POST["is_active"]

    if user_type == "staff":
        is_staff_value = request.POST['is_staff']
        is_superuser_value = request.POST['is_superuser']
        if is_staff_value == "true":
            is_staff = True

        if is_superuser_value == "true":
            is_superuser = True

    if is_active_value == "false":
        is_active = False
           
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    email_address = request.POST['email_address']
    mobile_number = request.POST['mobile_number']    

    check_email = User.objects.filter(email=email_address)
    check_email = check_email.exclude(email="")
    check_mobile = User_Profile.objects.filter(phone_number=mobile_number)
    check_mobile = check_mobile.exclude(phone_number="")


    if len(check_email) > 0:
        return HttpResponse("exists")

    elif len(check_mobile) > 0:            
        return HttpResponse("exists")

    else:
        try:
            user = User.objects.create(first_name=first_name, last_name=last_name, email=email_address, is_staff=is_staff, is_superuser=is_superuser, is_active=is_active)
            if mobile_number == "":
                mobile_number = None

            if email_address == "":
                email_address = None

            if email_address != "":
                user.username = email_address

            if email_address == "" and mobile_number != "":
                user.username = mobile_number
            
            profile = User_Profile.objects.create(user=user, phone_number=mobile_number)            
            user.save()
            profile.save()

            return HttpResponse(str(user.id)+":"+str(first_name)+":"+str(last_name)+":"+str(email_address)+":"+str(mobile_number)+":"+str(is_superuser)+":"+str(is_active))

        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add User: "+str(e))
            return HttpResponse("error")

@login_required
def edit_user_form(request, user_id):
    template_context = {}

    if request.user.is_superuser:
        template_context['superuser'] = True

    try:
        user = User.objects.get(id=user_id)
        profile = User_Profile.objects.get(user=user)
        if user.is_staff:
            template_context['is_staff'] = True
        else:
            template_context['is_staff'] = False

        template_context['user'] = user
        template_context['profile'] = profile
        return render(request, "staff/edit_user.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message="Edit User Form: "+str(e))
        return HttpResponse("failed")

@login_required
def edit_user(request):
    try:
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email_address = request.POST['email_address']
        mobile_number = request.POST['mobile_number']
        user_id = request.POST['user_id']
        is_active_value = request.POST['is_active']
        is_superuser_value = request.POST['is_superuser']

        user = User.objects.get(id=user_id)
        user_profile = User_Profile.objects.get(user=user)

        if is_superuser_value == "true":            
            user.is_superuser = True
        else:
            user.is_superuser = False

        if is_active_value == "true":
            user.is_active = True
        else:
            user.is_active = False
        
        user.first_name = first_name
        user.last_name = last_name
        user.email_address = email_address
        user_profile.phone_number = mobile_number

        user.save()
        user_profile.save()

        return HttpResponse("success")

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit User: "+str(e))
        return HttpResponse("error: "+str(e))

@login_required
def delete_user(request):
    user_id = request.POST['user_id']
    try:
        user = User.objects.get(id=user_id)
        user.delete()
        return HttpResponse("deleted")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Delete User: "+str(e))
        return HttpResponse("failed")



@login_required
def reports_page(request):
    """
        Manage Reports page
    """
    template_context = {}
    return render(request, "staff/reports.html", template_context)

@login_required
def payments_page(request):
    """
        Manage Payments page
    """
    template_context = {}
    return render(request, "staff/payments.html", template_context)

@login_required
def categories_page(request):
    template_context = {}
    if request.user.is_staff:
        template_context['categories'] = Category.objects.all()
        return render(request,"staff/categories.html",template_context)            
    else:
        return redirect("/staff/logout/")
        
@login_required
def add_category(request):    
    category_name = request.POST['category_name']
    category_description = request.POST['category_description']

    try:
        check_exists = Category.objects.filter(name=category_name, description=category_description)
        if check_exists:
            return HttpResponse("exists")
        else:
            category = Category.objects.create(name=category_name,description=category_description)
            message = str(category.id)+":"+str(category_name)+":"+str(category_description)
            return HttpResponse(message)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Category: "+str(e))
        return HttpResponse("failed")
 

@login_required
def edit_category_form(request,category_id):
    template_context = {}

    try:
        category = Category.objects.get(id=category_id)
        template_context['category'] = category
        return render(request,"staff/edit_category.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit Category: "+str(e))
        return HttpResponse("failed")

@login_required
def edit_category(request):
    category_id = request.POST['category_id']
    category_name = request.POST['category_name']
    category_description = request.POST['category_description']

    try:
        #check if the name already exists
        check_exists = Category.objects.filter(name=category_name)
        check_exists = check_exists.exclude(id=category_id)

        if check_exists:
            return HttpResponse("exists")
        else:
            category = Category.objects.get(id=category_id)
            category.name = category_name
            category.description = category_description
            category.save()
            return HttpResponse("success")

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit Category: "+str(e))
        return HttpResponse("failed")
    


@login_required
def delete_category(request):
    category_id = request.POST['category_id']
    try:
        category = Category.objects.get(id=category_id)
        category.delete()
        return HttpResponse("deleted")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Delete Category: "+str(e))
        return HttpResponse("failed")

@login_required
def ad_cabinet(request):
    template_context = {}
    try:
        ads_list = []
        #get adverts that have been added for the last three days
        #adverts = Advert.objects.filter(timestamp__gte=(timezone.now() - timedelta(days=3)))
        adverts = Advert.objects.all()
            
        for advert in adverts:    
            ad_info = {}
            ad_file = File(advert.advert_file)   
            print ad_file
            ad_info['advert'] = advert
            ad_info['id'] = advert.id
            ad_info['ad_url'] = ad_file#advert.advert_file#ad_file
            ad_info['orientation'] = advert.orientation
            ad_info['size'] = advert.size
            ad_info['user'] = advert.user
            ad_info['category'] = advert.category
            ad_info['stage'] = advert.booking_stage
            ads_list.append(ad_info)    

        template_context['categories'] = Category.objects.all()
        template_context['locations'] = Location.objects.all()
        template_context['ads_list'] = ads_list

        return render(request, "staff/ads.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Cabinet: "+str(e))
        return HttpResponse("error"+str(e))

@login_required
def search_ad_cabinet(request, location=None, category=None, start_date=None, end_date=None):
    ads_list = []
    adverts = Advert.objects.all()

    if not request.user.is_staff:
        adverts = adverts.filter(user=request.user)

    if category:
        adverts = adverts.filter(category=category)

    if start_date:
        adverts = adverts.filter(timestamp__gte=start_date, timestamp__lte=end_date)   
    else:
        adverts = adverts.filter(timestamp__gte=(timezone.now() - timedelta(days=3)))
        
            
    for advert in adverts:    
        ad_info = {}
        ad_file = File(advert.advert_file)   
        ad_info['advert'] = advert
        ad_info['id'] = advert.id
        ad_info['ad_url'] = ad_file
        ad_info['orientation'] = advert.orientation
        ad_info['size'] = advert.size
        ad_info['user'] = advert.user
        ad_info['category'] = advert.category
        ad_info['stage'] = advert.booking_stage
        ads_list.append(ad_info)  

    return ads_list

#book AD
@login_required
def book_ad_form(request):
    template_context = {}
    template_context['categories'] = get_categories_list(request)
    template_context['locations'] = Location.objects.all()
    template_context['durations'] = DURATION
    template_context['ad_sizes'] = AD_SIZE
    template_context['ad_orientation'] = AD_ORIENTATION
    template_context['ad_paper_type'] = AD_PAPER_TYPE
    template_context['ad_paper_extra_options'] = AD_PAPER_EXTRA_OPTIONS
    template_context['daily_rates'] = show_daily_rates()
    template_context['weekly_rates'] = show_weekly_rates()
    template_context['monthly_rates'] = show_monthly_rates()
    template_context['yearly_rates'] = show_yearly_rates()      

    clients_list = []
    for user in User.objects.filter(Q(is_superuser=False), Q(is_staff=False)):  
        user_profile = User_Profile.objects.filter(user=user)
        if len(user_profile) == 0:
            #create profile
            user_prof = User_Profile.objects.create(user=user)
        else:
            user_prof = User_Profile.objects.get(user=user)
        client_info = {}
        client_info['client'] = user
        client_info['phone_number'] = user_prof.phone_number

        clients_list.append(client_info)
    template_context['clients_list'] = clients_list

    #pricing
    price = Pricing.objects.get(id=1)
    template_context.update(get_pricing_information(price)) 

    return render(request, "staff/post_ad.html", template_context)

@login_required
def post_ad_step_1(request):

    template_context = {}

    user = ""
    ad_id = ""

    print_option = False
    paper_extra = ""

    post = request.POST
    cat_id = post['ad_category']

    category = Category.objects.get(id=cat_id)
    orientation = post['ad_orientation']
    size = post['ad_size']
    paper_type = post['ad_paper_type']

    if "ad_print" in request.POST:
        print_option = True

    if "ad_paper_extra_options" in request.POST:
        paper_extra = "Laminate"

    
    title = post['ad_title']
    description = post['ad_description']   


    try:

        if "update" in request.POST:
            ad_id = request.POST['ad_id']
            advert = Advert.objects.get(id=ad_id)
            advert.category = category
            advert.orientation = orientation
            advert.size = size
            advert.paper_type = paper_type
            advert.paper_extra_options = paper_extra
            advert.title = title
            advert.description = description
            advert.print_ad = print_option

            if "ad_file" in request.FILES:
                try:
                    advert_file = request.FILES['ad_file']
                    advert.advert_file = advert_file
                except:
                    Error_Log.objects.create(problem_type="INTERNAL", problem_message="Advert File: "+str(e))
                    pass
            
            advert.save()
            
        else:
            if "existing_user" in request.POST['choose_user']:
                client_id = request.POST['selected_client']
                user = User.objects.get(id=client_id)

            else:
                first_name = post['first_name']
                last_name = post['last_name']
                email_address = post['email_address']
                mobile_number = post['mobile_number']

                if len(email_address) > 0:
                    user = User.objects.create_user(first_name=first_name, last_name=last_name, email=email_address, password=email_address, username=email_address)

                else: 
                    user = User.objects.create_user(first_name=first_name, last_name=last_name, email=email_address, password=mobile_number, username=mobile_number)

                user_profile = User_Profile.objects.create(user=user)
                user_profile.phone_number = mobile_number
                user_profile.save()


            advert = Advert.objects.create(user=user, category=category, orientation=orientation, size=size,paper_type=paper_type,
                paper_extra_options=paper_extra, title=title, description=description, print_ad=print_option)

            if "ad_file" in request.FILES:
                try:
                    advert_file = request.FILES['ad_file']
                    advert.advert_file=advert_file
                    advert.save()
                except:
                    Error_Log.objects.create(problem_type="INTERNAL", problem_message="Advert File: "+str(e))
                    pass


            ad_id = str(advert.id)
        
        return HttpResponse(ad_id)
    
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Post AD 1: "+str(e))
        return HttpResponse("error")

@login_required
def post_ad_step_2(request):
    post = request.POST

    try:
        ad_id = post['ad_id']
        ad_locations = post['location_date_summary']

        advert = Advert.objects.get(id=ad_id)

        decoded_locations = json.loads(ad_locations)

        for loc in list(decoded_locations):
            try:
                location_id = loc['locationID']
                start_date = loc['startDate']
                end_date = loc['endDate']
                quantity = loc['quantity']
                price = loc['price']
                location = Location.objects.get(name=location_id)

                booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, end_date=end_date, quantity=quantity, price=price)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Booked AD: "+str(e))

        advert.booking_stage = "Stage2"
        advert.save()
        return HttpResponse(str(advert.id))
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Post AD 2: "+str(e))
        return HttpResponse("error")

@login_required
def post_ad_step_3(request):
    post = request.POST
    print "POST REQUEST"
    print post

    payment_mobile_number = ""
    payment_confirmation_code = ""

    try:
        ad_id = post['ad_id']
        amount = post['amount']
        payment_method = post["payment_option"]
        if payment_method == "mpesa":
            payment_mobile_number = post["payment_mobile_number"]
            payment_confirmation_code = post["payment_confirmation_code"]
            payment_method = "M-PESA"
                    
        advert = Advert.objects.get(id=ad_id)        
        print advert

        #check if payment exists and by pass it
        payments = Payment.objects.filter(advert=advert)
        if payments.count() > 0:
            pass
        else:
            Payment.objects.create(method=payment_method, advert = advert, amount=amount, user=advert.user)
        
        print amount
        print advert.grand_price
        advert.booking_stage = "Stage3"
        advert.grand_price = amount
        advert.save()
        print advert.grand_price
        print "+========+++++++++++++++++++++++++++++++++++++++++++++++++"

        #return redirect("/staff/ads/")
        return HttpResponse("success")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Post AD 3: "+str(e))
        return HttpResponse("error")
    

@login_required
def post_ad(request):
    template_context = {}
    post = request.POST    

    #AD details
    cat_id = post['ad_category']
    category = Category.objects.get(id=cat_id)
    orientation = post['ad_orientation']
    size = post['ad_size']
    title = post['ad_title']
    description = post['ad_description']    
    customer_type = post["customer_type"]

    if customer_type == "existing":
        user_id = post["selected_client"]
        user = User.objects.get(id=client_id)        

    else:
        first_name = post["first_name"]
        last_name = post["last_name"]
        email_address = post["email_address"]
        mobile_number = post["mobile_number"]

        user = User.objects.create(first_name=first_name, last_name=last_name, email=email_address, username=email_address, is_staff=False, is_superuser=False, is_active=True)
        profile = User_Profile.objects.create(user=user, phone_number=mobile_number)            
        user.save()
        profile.save()

    advert = Advert.objects.create(user=user, category=category, size=size, orientation=orientation, title=title, description=description)


    payment_method = post["payment_method"]
    if payment_method == "mpesa":
        payment_mobile_number = post["payment_mobile_number"]
        payment_confirmation_code = post["payment_confirmation_code"]
        payment_method = "M-PESA"
        
    else:
        amount = post["cash_amount"]
        payment_method = "M-PESA"
               
    try:
        advert_file = request.FILES['ad_file']
        advert.advert_file = advert_file
    except Exception as e:
        pass


    #AD locations
    ad_locations = post['location_date_summary']

    try:
        advert.save()

        decoded_locations = json.loads(ad_locations)

        for loc in list(decoded_locations):
            try:
                location_id = loc['locationID']
                start_date = loc['startDate']
                end_date = loc['endDate']
                quantity = loc['quantity']
                location = Location.objects.get(name=location_id)

                booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, end_date=end_date, quantity=quantity)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Booked AD: "+str(e))            
        return redirect("/staff/adverts/")
    except Exception as e:
        return redirect("/staff/adverts/")

@login_required
def staff_transactions(request):
    template_context = {}    

    try:
        start = datetime.now() - timedelta(days=7)
        end = datetime.now()
        payments = get_payments(request.user, None, None)

        template_context['payments'] = payments[0]
        template_context['all_payments'] = payments[1]
        return render(request, "staff/transactions.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Staff Transactions: "+str(e))
        return HttpResponse("Error showing your transactions")  

@login_required
def staff_payments(request):
    template_context = {}    

    try:
        if "start_date" in request.POST:
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            payments = get_payments(request.user, start_date, end_date)
            template_context['payments'] = payments[0]
            template_context['all_payments'] = payments[1]

        else:
            payments = get_payments(request.user, None, None)
            template_context['payments'] = payments[0]
            template_context['all_payments'] = payments[1]

        return render(request, "staff/payments.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Staff Payments: "+str(e))
        return HttpResponse("Error showing payments")  


@login_required
def staff_invoices(request):
    template_context = {}    

    try:
        if "start_date" in request.POST:
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            payments = get_payments(request.user, start_date, end_date)
            template_context['all_payments'] = payments[1]
        else:
            payments = get_payments(request.user, None, None)
            template_context['all_payments'] = payments[1]

        return render(request, "staff/invoices.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Staff Payments: "+str(e))
        return HttpResponse("Error showing invoices")  

@login_required
def invoice_page(request, invoice_id):
    template_context = {}
    try:
        invoice = Payment.objects.get(id=invoice_id)
        booked_ads = Booked_Advert.objects.filter(advert=invoice.advert)
        total = 0
        for ad in booked_ads:
            total += int(ad.quantity)
        template_context['invoice'] = invoice
        template_context['total'] = total
        return render(request, "staff/invoice_page.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Invoice Page: "+str(e))
        return HttpResponse("Error showing invoice page")


