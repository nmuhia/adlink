from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from models import User_Profile
from django.contrib.auth.forms import UserChangeForm
from django import forms

class UserInline(admin.StackedInline):
    model = User_Profile
    can_delete = False
    verbose_name_plural = 'profile'
    list_display = ('phone_number',)

class UserAdmin(UserAdmin):
    inlines = (UserInline, )


admin.autodiscover()
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
