from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password, check_password

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.context_processors import csrf
from django.core import serializers

from django.http import HttpResponse, Http404
from django.core.management import call_command

from django.conf import settings

import json
from datetime import datetime, timedelta

from Payments.models import Payment
from Profiles.models import User_Profile, SOCIAL_ACCOUNT
from Adverts.models import Advert, Booked_Advert, Category, Pricing, Error_Log, AD_SIZE, AD_ORIENTATION, AD_PAPER_TYPE, AD_PAPER_EXTRA_OPTIONS
from Locations.models import Location, Space, DURATION

from Locations.views import show_daily_rates, show_weekly_rates, show_monthly_rates, show_yearly_rates
from Adverts.views import get_categories_list, show_ads, show_booked_ads, get_pricing_information, get_ad_details, get_booked_ad_details
from Payments.views import get_payments


def home_page(request):
    if request.user.is_authenticated():
        if request.user.is_staff:
            if request.user.is_authenticated():
                return redirect("/staff/dashboard/")
            else:
                return redirect("/staff/")
        else:
            template_context = {}
            template_context = default_data(request)
            return redirect("/user/dashboard/")

    else:
        template_context = {}
        return render(request,"users/home.html",template_context) 

def register_user(request):
    first_name =  request.POST['first_name']
    last_name = request.POST['last_name']
    mobile_phone = request.POST['mobile_phone']
    email_address = request.POST['email_address']
    password = request.POST['password']

    #create hashed password
    hashed_password = make_password(password, salt=None, hasher='default')

    try:        
        #check if email exists
        check_email_exists = User.objects.filter(email=email_address)
        check_email_exists = check_email_exists.exclude(email="")

        #check if phone exists
        check_phone_exists = User_Profile.objects.filter(phone_number=mobile_phone)
        check_phone_exists = check_phone_exists.exclude(phone_number="")

        if len(check_email_exists) > 0:
            return HttpResponse("Email already exists, please choose a different one.")

        elif len(check_phone_exists) > 0:
            return HttpResponse("Phone number already exists, please choose a different one.")

        else:
            if email_address != "":
                user = User.objects.create_user(first_name=first_name, last_name=last_name, username=email_address, email=email_address, password=password)
                user_profile = User_Profile.objects.create(user_id=user.id)
                if mobile_phone != "":                    
                    user_profile.phone_number = mobile_phone
                    user_profile.save()
            else:
                user = User.objects.create_user(first_name=first_name, last_name=last_name, username=mobile_phone, password=password)   

                user_profile = User_Profile.objects.create(user_id=user.id)
                user_profile.phone_number = mobile_phone

                user_profile.save()
            
        return HttpResponse("Your account has been created successfully.")
    except Exception as e:
        return HttpResponse("There was an error creating your account: "+str(e))

def login_user(request):
    username = request.POST["username"] 
    password = request.POST["password"]
   
    try:
        user = authenticate(username=username, password=password)

        if user is not None and not user.is_staff:
            print "user is not none"
            if user.is_active:                
                login(request, user)
                return redirect("/user/dashboard/")
            else:
                return HttpResponse("disabled")
        else:            
            return HttpResponse("fail")
    except Exception as e:
        print "Error: "+str(e)
        return HttpResponse("fail")

@login_required
def user_dashboard(request):
    template_context = {}    

    try:
        template_context['ads'] = Advert.objects.filter(user=request.user)
        template_context['ads_list'] = show_ads(request)
        return render(request, "users/dashboard.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="User Dashboard: "+str(e))
        return HttpResponse("Error showing your dashboard")

@login_required
def user_profile(request):
    template_context = {}    

    try:
        template_context = default_data(request)
        template_context['social_accounts'] = SOCIAL_ACCOUNT
        return render(request, "users/profile.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="User Profile: "+str(e))
        return HttpResponse("Error showing your profile")

@login_required
def edit_user_profile(request):
    template_context = {}
    try:
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email_address = request.POST['email_address']
        phone_number = request.POST['phone_number']

        user = User.objects.get(id=request.user.id)
        user.first_name = first_name
        user.last_name = last_name
        user.email = email_address

        profile = User_Profile.objects.get(user=user)
        profile.phone_number = phone_number

        user.save()
        profile.save()
        return HttpResponse("success")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit User Profile: "+str(e))
        return HttpResponse("error")
    

@login_required
def user_ads(request):
    template_context = {} 

    try:
        template_context = default_data(request)
        template_context['categories'] = get_categories_list(request)
        template_context['locations'] = Location.objects.all()
        template_context['ads_list'] = show_ads(request)
        template_context['booked_ads_list'] = show_booked_ads(request)
        return render(request, "users/ads.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="User AD Cabinet: "+str(e))
        return HttpResponse("Error showing your ADs")

@login_required
def ad_details(request, ad_id):
    """
        Return AD details page
    """
    template_context = {}
    try:
        template_context['advert'] = get_ad_details(ad_id)
        return render(request, "users/ad_details.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Detail: "+str(e))
        return HttpResponse("Error showing your AD")

@login_required
def booked_ad_details(request, booking_ad_id):
    """
        Return AD details page
    """
    template_context = {}
    try:
        template_context.update(get_booked_ad_details(booking_ad_id))
        return render(request, "users/booked_ad_details.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Detail: "+str(e))
        return HttpResponse("Error showing your AD")


@login_required
def book_ad(request):    
    template_context = {} 

    try:
        template_context = default_data(request)
        template_context['categories'] = Category.objects.all()
        template_context['locations'] = Location.objects.all()
        template_context['durations'] = DURATION
        template_context['ad_sizes'] = AD_SIZE
        template_context['ad_orientation'] = AD_ORIENTATION
        template_context['ad_paper_type'] = AD_PAPER_TYPE
        template_context['ad_paper_extra_options'] = AD_PAPER_EXTRA_OPTIONS
        template_context['daily_rates'] = show_daily_rates()
        template_context['weekly_rates'] = show_weekly_rates()
        template_context['monthly_rates'] = show_monthly_rates()
        template_context['yearly_rates'] = show_yearly_rates()
        
        #pricing
        price = Pricing.objects.get(id=1)
        template_context.update(get_pricing_information(price))        

        return render(request, "users/post_ad.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Book AD: "+str(e))
        return HttpResponse("Error booking AD") 

@login_required
def post_ad_step_1(request):
    template_context = {}
    post = request.POST

    #AD details
    cat_id = post['ad_category']

    category = Category.objects.get(id=cat_id)
    orientation = post['ad_orientation']
    size = post['ad_size']
    paper_type = post['ad_paper_type']
    #paper_extra_options = None
    paper_extra_options = ""
    title = post['ad_title']
    description = post['ad_description']
    contact_name = post['contact_name']
    contact_email = post['contact_email_address']
    contact_phone = post['contact_mobile_number']
    advert_file = request.FILES['ad_file']

    try:
        if 'update' in request.POST:
            ad_id = request.POST['ad_id']
            advert = Advert.objects.get(id=ad_id)
            advert.category = category
            advert.orientation = orientation
            advert.size = size
            advert.paper_type = paper_type
            advert.paper_extra_options = paper_extra_options
            advert.title = title
            advert.description = description
            advert.contact_name = contact_name
            advert.contact_phone = contact_phone
            advert.contact_email = contact_email
            advert.advert_file = advert_file
            advert.save()

            return HttpResponse(str(advert.id))
        else:
            #create new AD
            advert = Advert.objects.create(user=request.user, category=category, orientation=orientation, size=size,paper_type=paper_type,
                paper_extra_options=paper_extra_options, title=title, description=description, 
                contact_name=contact_name, contact_phone=contact_phone, contact_email=contact_email, 
                advert_file=advert_file)
          
            return HttpResponse(str(advert.id))
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Post AD: "+str(e))
        return redirect("error")

@login_required
def post_ad_step_2(request):
    print "step 2"

@login_required
def post_ad_step_3(request):
    print "step 3"

"""def post_ad(request):
    template_context = {}
    post = request.POST

    #AD details
    cat_id = post['ad_category']

    category = Category.objects.get(id=cat_id)
    orientation = post['ad_orientation']
    size = post['ad_size']
    paper_type = post['ad_paper_type']
    #paper_extra_options = None
    paper_extra_options = ""
    title = post['ad_title']
    description = post['ad_description']
    contact_name = post['contact_name']
    contact_email = post['contact_email_address']
    contact_phone = post['contact_mobile_number']
    advert_file = request.FILES['ad_file']
    grand_price = post['total_price']

    #AD locations
    ad_locations = post['location_date_summary']

    #AD payments
    ad_payment_mobile_number = post['payment_mobile_number']
    ad_payment_confirmation_code = post['payment_confirmation_code']

    try:
        advert = Advert.objects.create(user=request.user, category=category, orientation=orientation, size=size,paper_type=paper_type,
            paper_extra_options=paper_extra_options, title=title, description=description, 
            contact_name=contact_name, contact_phone=contact_phone, contact_email=contact_email, 
            advert_file=advert_file, grand_price=grand_price)

        decoded_locations = json.loads(ad_locations)
        print decoded_locations

        for loc in list(decoded_locations):
            try:
                location_id = loc['locationID']
                start_date = loc['startDate']
                end_date = loc['endDate']
                quantity = loc['quantity']
                price = loc['price']                
                location = Location.objects.get(name=location_id)

                booked_ad = Booked_Advert.objects.create(advert=advert, location=location, start_date=start_date, end_date=end_date, quantity=quantity, price=price)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Booked AD: "+str(e))

        #create payment
        try:
            payment = Payment.objects.create(raw_message="",amount=grand_price, user=request.user, reference=ad_payment_confirmation_code, advert=advert)
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Payment: "+str(e))
  
        return redirect("/user/ads/")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Post AD: "+str(e))
        return redirect("/user/ads/")"""
        

@login_required
def user_transactions(request):
    template_context = {}    

    try:
        start = datetime.now() - timedelta(days=7)
        end = datetime.now()

        template_context['payments'] = get_payments(request.user, None, None)
        template_context['all_payments'] = Payment.objects.filter(user=request.user)
        print template_context
        return render(request, "users/transactions.html",template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="User Transactions: "+str(e))
        return HttpResponse("Error showing your transactions")                

@login_required
def user_logout(request):
    try:
        logout(request)
        return redirect("/")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Logout: "+str(e))
        return HttpResponse("")

@login_required
def default_data(request):
    template_context = {}
    profile = User_Profile.objects.get(user_id=request.user.id)
    template_context['user'] = request.user
    template_context['profile'] = profile
    template_context['daily_rates'] = show_daily_rates()
    return template_context

