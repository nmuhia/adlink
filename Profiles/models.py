from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

SOCIAL_ACCOUNT = (
    ('Facebook','<i class="fa fa-facebook"></i>'),
    ('Twitter','<i class="fa fa-twitter"></i>'),
    ('Google +','<i class="fa fa-google-plus"></i>'),
)

JOB_DESCRIPTION = (
    ('FD', 'Front Desk Personnel'),
    ('D', 'Delivery'),
)

class User_Profile(models.Model):
    user = models.OneToOneField(User)
    phone_number = models.CharField(max_length=20, blank=True, null=True, default=None)
    country = models.CharField(max_length=100, blank=True, null=True, default=None)
    background_image = models.FileField(upload_to='backgrounds', blank=True, null=True)
    profile_image = models.FileField(upload_to='profiles', blank=True, null=True)
    job_description = models.CharField(max_length=2, choices=JOB_DESCRIPTION, blank=True, null=True, default='')
    

    def __unicode__(self):
        return self.user
    
    class Meta:
        verbose_name = "User Profile"


class Social_Account(models.Model):
    user = models.OneToOneField(User)
    link = models.CharField(max_length=200, blank=True, null=True, default=None)
    icon = models.CharField(max_length=50, blank=True, null=True, default=None)
    
    def __unicode__(self):
        return self.link

    class Meta:
        verbose_name = "Social Account"
