from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings

admin.autodiscover()

#user functions
urlpatterns = patterns('Profiles.users',
    #user functions
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','home_page'),
    url(r'^register/user/$','register_user'),
    url(r'^login/user/$','login_user'),
    url(r'^user/dashboard/$','user_dashboard'),
    url(r'^user/profile/$','user_profile'),
    url(r'^user/profile/edit/$','edit_user_profile'),
    url(r'^user/ads/$','user_ads'),
    url(r'^user/ads/(.+)/$','ad_details'),
    url(r'^user/ad/booking/(.+)/$','booked_ad_details'),
    url(r'^user/ad/book/$','book_ad'),
    url(r'^user/ad/post/1/$','post_ad_step_1'),
    url(r'^user/ad/post/2/$','post_ad_step_2'),
    url(r'^user/ad/post/3/$','post_ad_step_3'),
    url(r'^user/transactions/$','user_transactions'),
    url(r'^user/logout/$','user_logout'),
    #for python social auth
    url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include('django.contrib.auth.urls', namespace='auth')),#logging in and out of the app multiple times
)

#superuser/staff functions
urlpatterns += patterns('Profiles.staff',
    url(r'^staff/$','login_form'),
    url(r'^staff/login/$','login_page'),
    url(r'^staff/logout/$','logout_page'),
    url(r'^staff/forgot/$','forgot_password_page'),
    url(r'^staff/dashboard/$','dashboard_page'),
    url(r'^staff/adverts/$','adverts_page'),
    url(r'^staff/customers/$','customers_page'),
    url(r'^staff/customers/add/form/$','add_customer_form'),
    url(r'^staff/customers/add/$','add_customer'),
    url(r'^staff/personnel/$','personnel_page'),
    url(r'^staff/personnel/add/form/$','add_personnel_form'),
    url(r'^staff/personnel/add/$','add_personnel'),
    
    url(r'^staff/users/staff/$','staff_page'),
    url(r'^staff/users/edit/$','edit_user'),
    url(r'^staff/users/edit/(.+)/$','edit_user_form'),
    url(r'^staff/users/delete/$','delete_user'),
    url(r'^staff/payments/$','payments_page'),
    url(r'^staff/reports/$','reports_page'),
    url(r'^staff/categories/$','categories_page'),
    url(r'^staff/categories/add/$','add_category'),
    url(r'^staff/categories/edit/(.+)/$','edit_category_form'),
    url(r'^staff/categories/edit/$','edit_category'),
    url(r'^staff/categories/delete/$','delete_category'),

    #book AD for staff
    url(r'^staff/ad/book/$','book_ad_form'),
    url(r'^staff/ad/post/1/$','post_ad_step_1'),
    url(r'^staff/ad/post/2/$','post_ad_step_2'),
    url(r'^staff/ad/post/3/$','post_ad_step_3'),
    #url(r'^staff/ad/post/$','post_ad'),
    url(r'^staff/ads/filter/$','filter_ads'),
    url(r'^staff/ads/filter/reset/$','reset_filter_ads'),
    url(r'^staff/ads/$','ad_cabinet'),
    url(r'^staff/ads/(.+)/$','ad_info'),
    url(r'^staff/ad/booking/(.+)/$','booked_ad_details'),
    #url(r'^staff/users/(.+)/$','user_info'),

    #TRANSACTIONS
    url(r'^staff/transactions/$','staff_transactions'),
    url(r'^staff/transactions/payments/$','staff_payments'),
    url(r'^staff/transactions/invoices/$','staff_invoices'),
    url(r'^staff/transactions/invoices/(.+)/$','invoice_page'),
    
    #test check num spaces available

) 

urlpatterns += patterns('Locations.views',
    #locations
    url(r'^staff/locations/$','locations_page'),
    url(r'^staff/locations/add/form/$','add_location_form'),
    #url(r'^staff/locations/create/$','create_location'),
    url(r'^staff/locations/add/$','add_location'),
    #url(r'^staff/locations/edit/(.+)/$','edit_location_form'),
    #url(r'^staff/locations/edit/$','edit_location'),"""
    url(r'^staff/locations/delete/$','delete_location'),
    #sections
    url(r'^staff/sections/$','sections_page'),
    url(r'^staff/sections/add/form/$','add_section_form'),
    url(r'^staff/sections/add/$','add_section'),
    #spaces
    url(r'^staff/sections/(.+)/$','manage_spaces_page'),
    url(r'^staff/spaces/add/$', 'add_space'),
    #inventory
    url(r'^staff/inventory/$','inventory_page'),
    url(r'^staff/inventory/add/(.+)/$','add_inventory_page'),
    url(r'^staff/inventory/view/(.+)/$','view_inventory_page'),
    #url(r'^staff/inventory/add/form/$','add_inventory_page'),
    url(r'^staff/inventory/create/$','create_inventory'),
    url(r'^staff/inventory/upload_csv/$','upload_csv_inventory'),
    url(r'^staff/inventory/search/$','search_inventory'),
    #url(r'^staff/inventory/search_results/$','search_results_inventory'),

    #job schedule
    url(r'^staff/jobs/$', 'print_jobs_page'),
    url(r'^staff/jobs/print/$', 'print_jobs_page'),
    url(r'^staff/jobs/delivery/$', 'delivery_jobs_page'),
    url(r'^staff/jobs/posting/$', 'posting_jobs_page'),
    url(r'^staff/jobs/removal/$', 'removal_jobs_page'),

    #space_management
    url(r'available_spaces/$','available_spaces'),
    url(r'delete_reservations/$','delete_reservations'),

    #check spaces
    url(r'check_spaces/$','check_spaces'),
)

urlpatterns += patterns('Adverts.views',
    #print pricing
    url(r'^staff/adverts/pricing/$','pricing_page'), 
    url(r'^staff/adverts/pricing/edit/$','edit_pricing'), 

    #discounts
    url(r'^staff/discounts/$','discounts_page'), 
    url(r'^staff/discounts/add/$','add_discount_form'),
    url(r'^staff/discounts/create/$','add_discount'), 


)

#media and static files
urlpatterns += patterns('',
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)
urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', { 'document_root': settings.MEDIA_ROOT}),
)

#help desk url
urlpatterns += patterns('',
        url(r'support/', include('helpdesk.urls')),
)
