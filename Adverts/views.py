from django.contrib.auth.decorators import login_required

from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.core.files import File


from models import Advert, Booked_Advert, Pricing, Discount, Category, Error_Log, AD_SIZE, PERCENTAGE_OF, DISCOUNT_TYPE
from Locations.models import Location, DURATION


def get_pricing_information(price):
    template_context = {}

    template_context["A1_normal_price"] = price.A1_normal_price
    template_context["A2_normal_price"] = price.A2_normal_price
    template_context["A3_normal_price"] = price.A3_normal_price
    template_context["A4_normal_price"] = price.A4_normal_price
    template_context["A5_normal_price"] = price.A5_normal_price

    template_context["A1_gloss_price"] = price.A1_gloss_price
    template_context["A2_gloss_price"] = price.A2_gloss_price
    template_context["A3_gloss_price"] = price.A3_gloss_price
    template_context["A4_gloss_price"] = price.A4_gloss_price
    template_context["A5_gloss_price"] = price.A5_gloss_price

    template_context["A1_lamination_price"] = price.A1_lamination_price
    template_context["A2_lamination_price"] = price.A2_lamination_price
    template_context["A3_lamination_price"] = price.A3_lamination_price
    template_context["A4_lamination_price"] = price.A4_lamination_price
    template_context["A5_lamination_price"] = price.A5_lamination_price        
    
    return template_context


@login_required
def pricing_page(request):
    template_context = {}
    try:
        price = Pricing.objects.get(id=1)
        template_context.update(get_pricing_information(price))
        
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Pricing Page:"+str(e))
        
        #create a new pricing and set prices to zero
        price = Pricing.objects.create(A1_normal_price="0",
                               A2_normal_price="0",
                               A3_normal_price="0",
                               A4_normal_price="0",
                               A5_normal_price="0",
                               A1_gloss_price="0",
                               A2_gloss_price="0",
                               A3_gloss_price="0",
                               A4_gloss_price="0",
                               A5_gloss_price="0",
                               A1_lamination_price="0",
                               A2_lamination_price="0",
                               A3_lamination_price="0",
                               A4_lamination_price="0",
                               A5_lamination_price="0"
        )
        template_context.update(get_pricing_information(price))
                
    return render(request,"staff/pricing.html",template_context)

def edit_pricing(request):
    post = request.POST
    
    try:
        a1_n = post['a1_n']
        a2_n = post['a2_n']
        a3_n = post['a3_n']
        a4_n = post['a4_n']
        a5_n = post['a5_n']
        a1_g = post['a1_g']
        a2_g = post['a2_g']
        a3_g = post['a3_g']
        a4_g = post['a4_g']
        a5_g = post['a5_g']
        a1_l = post['a1_l']
        a2_l = post['a2_l']
        a3_l = post['a3_l']
        a4_l = post['a4_l']
        a5_l = post['a5_l']

        pricing = Pricing.objects.get(id=1)
        pricing.A1_normal_price = a1_n
        pricing.A2_normal_price = a2_n
        pricing.A3_normal_price = a3_n
        pricing.A4_normal_price = a4_n
        pricing.A5_normal_price = a5_n

        pricing.A1_gloss_price = a1_g
        pricing.A2_gloss_price = a2_g
        pricing.A3_gloss_price = a3_g
        pricing.A4_gloss_price = a4_g
        pricing.A5_gloss_price = a5_g

        pricing.A1_lamination_price = a1_l
        pricing.A2_lamination_price = a2_l
        pricing.A3_lamination_price = a3_l
        pricing.A4_lamination_price = a4_l
        pricing.A5_lamination_price = a5_l

        pricing.save()
        return HttpResponse("success")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Edit Pricing: "+str(e))
        return HttpResponse("failed")


@login_required
def discounts_page(request):
    template_context = {}
    try:
        template_context['duration_items'] = Discount.objects.filter(discount_type="by_duration")
        template_context['location_items'] = Discount.objects.filter(discount_type="by_location")
        template_context['num_location_items'] = Discount.objects.filter(discount_type="by_num_of_locations")
        return render(request, "staff/discounts.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Discounts Page: "+str(e))

@login_required
def add_discount_form(request):
    template_context = {}
    try:
        template_context['sizes'] = AD_SIZE
        template_context['duration'] = DURATION
        template_context['locations'] = Location.objects.all()
        template_context['percentage_of'] = PERCENTAGE_OF
        template_context['discounts'] = DISCOUNT_TYPE
        return render(request, "staff/create_discount.html", template_context)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Discount Form: "+str(e))
        return render(request, "staff/create_discount.html", template_context)

@login_required
def add_discount(request):
    try:
        option  = request.POST['discount_options']

        discount = Discount.objects.create()
        discount.discount_type = option             

        if option == "by_duration":
            from_range = request.POST['from_range']
            to_range = request.POST['to_range']
            duration = request.POST['duration']
            location_id = request.POST['location']
            percentage = request.POST['percentage']
            percentage_of = request.POST['percentage_of']
            size = request.POST['size']

            print from_range
            print to_range
            print duration
            print location_id
            print percentage
            print percentage_of
            print "========================================================"
            if len(from_range) > 0:
                discount.from_range = from_range
            if len(to_range) > 0:
                discount.to_range = to_range
            if len(duration) > 0:
                discount.duration = duration
            if len(location_id) > 0:      
                location = Location.objects.get(id=location_id)          
                discount.location = location
            if len(size) > 0:
                discount.ad_size = size
            if len(percentage) > 0:
                discount.percentage = percentage
            if len(percentage_of) > 0:
                discount.percentage_of = percentage_of            
            

        elif option == "by_location":
            start_date = request.POST['by_loc_start_date']
            end_date = request.POST['by_loc_end_date']
            location_id = request.POST['by_loc_location']
            size = request.POST['by_loc_size']
            percentage = request.POST['by_loc_percentage']
            percentage_of = request.POST['by_loc_percentage_of']

            if len(start_date) > 0:
                discount.start_date = start_date
            if len(end_date) > 0:
                discount.end_date = end_date
            if len(location_id) > 0:
                location = Location.objects.get(id=location_id)
                discount.location = location
            if len(size) > 0:
                discount.ad_size = size
            if len(percentage) > 0:
                discount.percentage = percentage
            if len(percentage_of) > 0:
                discount.percentage_of = percentage_of

        elif option == "by_num_of_locations":
            threshold = request.POST['by_num_threshold']
            start_date = request.POST['by_num_start_date']
            end_date = request.POST['by_num_end_date']
            percentage = request.POST['by_num_percentage']
            percentage_of = request.POST['by_num_percentage_of']

            if len(start_date) > 0:
                discount.start_date = start_date
            if len(end_date) > 0:
                discount.end_date = end_date
            if len(percentage) > 0:
                discount.percentage = percentage
            if len(percentage_of) > 0:
                discount.percentage_of = percentage_of
            if len(threshold) > 0:
                discount.threshold = threshold

        discount.save()

        return redirect("/staff/discounts/")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Add Discount: "+str(e))
        return redirect("/staff/discounts/")
    

def get_categories_list(request):    
    return Category.objects.all()

def show_ads(request):
    ads_list = []
    adverts = Advert.objects.all()
    if not request.user.is_staff:
        adverts = adverts.filter(user=request.user)       
        
    for advert in adverts:    
        ad_info = {}
        ad_file = File(advert.advert_file)   
        ad_info['advert'] = advert
        ad_info['ad_id'] = advert.id
        ad_info['ad_url'] = ad_file
        ads_list.append(ad_info)

    return ads_list

def show_booked_ads(request):
    booked_ads_list = []
    for advert in Advert.objects.filter(user=request.user):
        for booking in Booked_Advert.objects.filter(advert=advert):
            ad_info = {}
            ad_info['ad_id'] = advert.id
            ad_info['booking_id'] = booking.id
            ad_info['start_date'] = booking.start_date
            ad_info['end_date'] = booking.end_date
            ad_info['location'] = booking.location
            ad_info['category'] = advert.category
            ad_info['quantity'] = booking.quantity

            booked_ads_list.append(ad_info)
            
    return booked_ads_list


def get_advert_bookings(advert):
    booked_ads_list = []

    for booking in Booked_Advert.objects.filter(advert=advert):
        ad_info = {}
        ad_info['ad_id'] = advert.id
        ad_info['advert'] = advert
        ad_info['booking_id'] = booking.id
        ad_info['extended_booking_id'] = "B%05d" % int(booking.id)
        ad_info['booking'] = booking
        ad_info['start_date'] = booking.start_date
        ad_info['end_date'] = booking.end_date
        ad_info['location'] = booking.location
        ad_info['category'] = advert.category
        ad_info['quantity'] = booking.quantity

        booked_ads_list.append(ad_info)
            
    return booked_ads_list
    


def get_ad_details(ad_id):
    """
        Return details regarding this AD, including associated booked ADs
    """
    template_context = {}
    try:
        ad = Advert.objects.get(id=ad_id)

        ad_file = File(ad.advert_file)
        template_context['ad_url'] = ad_file        
        template_context['ad'] = ad
        template_context['id'] = "%05d" % int(ad_id)
        template_context['booked_ads'] = get_advert_bookings(ad)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Details: "+str(e))

    return template_context


def get_booked_ad_details(booked_ad_id):
    """
        Return details of this booked ADs
    """
    template_context = {}
    try:
        booked_ad = Booked_Advert.objects.get(id=booked_ad_id)

        ad_file = File(booked_ad.advert.advert_file)
        template_context['ad_url'] = ad_file        
        template_context['ad'] = booked_ad.advert
        template_context['booking'] = booked_ad
        template_context['extended_booking_id'] = "B%05d" % int(booked_ad_id)
        template_context['booking_id'] = booked_ad_id

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="AD Details: "+str(e))

    return template_context
