from celery import task
from django.core.management import call_command

@task
def toPrint():
    """
        Calls helper methods to print jobs that are scheduled for delivery, calls Google Cloud Print API
    """
    call_command('run_to_print')

@task
def toDeliver():
    """
        Calls helper methods to deliver jobs that are scheduled for delivery
    """
    call_command('run_to_remove')

@task
def toRemove():
    """
        Calls helper methods to remove jobs that are scheduled for removal
    """
    call_command('run_to_remove')
