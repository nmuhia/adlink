# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Advert.timestamp'
        db.add_column(u'Adverts_advert', 'timestamp',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Advert.timestamp'
        db.delete_column(u'Adverts_advert', 'timestamp')


    models = {
        u'Adverts.advert': {
            'Meta': {'object_name': 'Advert'},
            'advert_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'booking_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'booking_stage': ('django.db.models.fields.CharField', [], {'default': "'Stage1'", 'max_length': '100'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Adverts.Category']"}),
            'contact_email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'contact_phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grand_price': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orientation': ('django.db.models.fields.CharField', [], {'default': "'Potrait'", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'paper_extra_options': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'paper_type': ('django.db.models.fields.CharField', [], {'default': "'Normal'", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'size': ('django.db.models.fields.CharField', [], {'default': "'A1'", 'max_length': '30'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'Adverts.booked_advert': {
            'Meta': {'object_name': 'Booked_Advert'},
            'advert': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['Adverts.Advert']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Locations.Location']"}),
            'price': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '20'}),
            'quantity': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Ready'", 'max_length': '30'})
        },
        u'Adverts.category': {
            'Meta': {'ordering': "['name']", 'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'Adverts.discount': {
            'Meta': {'object_name': 'Discount'},
            'ad_size': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': "'Days'", 'max_length': '100'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'from_range': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Locations.Location']", 'null': 'True', 'blank': 'True'}),
            'percentage': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'percentage_of': ('django.db.models.fields.CharField', [], {'default': "'TP'", 'max_length': '100'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'threshold': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'to_range': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'Adverts.error_log': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'Error_Log'},
            'advert': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Adverts.Advert']", 'null': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_resolved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'problem_message': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'problem_type': ('django.db.models.fields.CharField', [], {'default': "'OTHER'", 'max_length': '30'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'Adverts.pricing': {
            'A1_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A1_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A1_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A2_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A2_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A2_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A3_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A3_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A3_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A4_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A4_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A4_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A5_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A5_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A5_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'Meta': {'object_name': 'Pricing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Locations.location': {
            'Meta': {'ordering': "['name']", 'object_name': 'Location'},
            'currency': ('django.db.models.fields.CharField', [], {'default': "'KES'", 'max_length': '100', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img1': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img2': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tax_rate': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'traffic': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Adverts']