# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Advert'
        db.create_table(u'Adverts_advert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Adverts.Category'])),
            ('size', self.gf('django.db.models.fields.CharField')(default='A1', max_length=30)),
            ('orientation', self.gf('django.db.models.fields.CharField')(default='Potrait', max_length=30, null=True, blank=True)),
            ('paper_type', self.gf('django.db.models.fields.CharField')(default='Normal', max_length=30, null=True, blank=True)),
            ('paper_extra_options', self.gf('django.db.models.fields.CharField')(default='', max_length=30, null=True, blank=True)),
            ('contact_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('contact_phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('contact_email', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('advert_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('booking_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True)),
            ('grand_price', self.gf('django.db.models.fields.CharField')(default='0', max_length=20)),
        ))
        db.send_create_signal(u'Adverts', ['Advert'])

        # Adding model 'Booked_Advert'
        db.create_table(u'Adverts_booked_advert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('advert', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['Adverts.Advert'], null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Locations.Location'])),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('quantity', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('price', self.gf('django.db.models.fields.CharField')(default='0', max_length=20)),
            ('status', self.gf('django.db.models.fields.CharField')(default='Ready', max_length=30)),
        ))
        db.send_create_signal(u'Adverts', ['Booked_Advert'])

        # Adding model 'Category'
        db.create_table(u'Adverts_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'Adverts', ['Category'])

        # Adding model 'Pricing'
        db.create_table(u'Adverts_pricing', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('A1_normal_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A2_normal_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A3_normal_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A4_normal_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A5_normal_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A1_gloss_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A2_gloss_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A3_gloss_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A4_gloss_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A5_gloss_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A1_lamination_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A2_lamination_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A3_lamination_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A4_lamination_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
            ('A5_lamination_price', self.gf('django.db.models.fields.CharField')(default=0, max_length=100, blank=True)),
        ))
        db.send_create_signal(u'Adverts', ['Pricing'])

        # Adding model 'Discount'
        db.create_table(u'Adverts_discount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('discount_type', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('from_range', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('to_range', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('duration', self.gf('django.db.models.fields.CharField')(default='Days', max_length=100)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Locations.Location'], null=True, blank=True)),
            ('threshold', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ad_size', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('percentage', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('percentage_of', self.gf('django.db.models.fields.CharField')(default='TP', max_length=100)),
            ('start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Adverts', ['Discount'])

        # Adding model 'Error_Log'
        db.create_table(u'Adverts_error_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('problem_type', self.gf('django.db.models.fields.CharField')(default='OTHER', max_length=30)),
            ('problem_message', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
            ('creator', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('is_resolved', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('advert', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Adverts.Advert'], null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
        ))
        db.send_create_signal(u'Adverts', ['Error_Log'])


    def backwards(self, orm):
        # Deleting model 'Advert'
        db.delete_table(u'Adverts_advert')

        # Deleting model 'Booked_Advert'
        db.delete_table(u'Adverts_booked_advert')

        # Deleting model 'Category'
        db.delete_table(u'Adverts_category')

        # Deleting model 'Pricing'
        db.delete_table(u'Adverts_pricing')

        # Deleting model 'Discount'
        db.delete_table(u'Adverts_discount')

        # Deleting model 'Error_Log'
        db.delete_table(u'Adverts_error_log')


    models = {
        u'Adverts.advert': {
            'Meta': {'object_name': 'Advert'},
            'advert_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'booking_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Adverts.Category']"}),
            'contact_email': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'contact_phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grand_price': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orientation': ('django.db.models.fields.CharField', [], {'default': "'Potrait'", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'paper_extra_options': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'paper_type': ('django.db.models.fields.CharField', [], {'default': "'Normal'", 'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'size': ('django.db.models.fields.CharField', [], {'default': "'A1'", 'max_length': '30'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'Adverts.booked_advert': {
            'Meta': {'object_name': 'Booked_Advert'},
            'advert': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['Adverts.Advert']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Locations.Location']"}),
            'price': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '20'}),
            'quantity': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Ready'", 'max_length': '30'})
        },
        u'Adverts.category': {
            'Meta': {'ordering': "['name']", 'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'Adverts.discount': {
            'Meta': {'object_name': 'Discount'},
            'ad_size': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'discount_type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': "'Days'", 'max_length': '100'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'from_range': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Locations.Location']", 'null': 'True', 'blank': 'True'}),
            'percentage': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'percentage_of': ('django.db.models.fields.CharField', [], {'default': "'TP'", 'max_length': '100'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'threshold': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'to_range': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'Adverts.error_log': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'Error_Log'},
            'advert': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Adverts.Advert']", 'null': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_resolved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'problem_message': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'problem_type': ('django.db.models.fields.CharField', [], {'default': "'OTHER'", 'max_length': '30'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'Adverts.pricing': {
            'A1_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A1_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A1_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A2_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A2_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A2_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A3_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A3_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A3_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A4_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A4_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A4_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A5_gloss_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A5_lamination_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'A5_normal_price': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '100', 'blank': 'True'}),
            'Meta': {'object_name': 'Pricing'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Locations.location': {
            'Meta': {'ordering': "['name']", 'object_name': 'Location'},
            'currency': ('django.db.models.fields.CharField', [], {'default': "'KES'", 'max_length': '100', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img1': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img2': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tax_rate': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'traffic': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Adverts']