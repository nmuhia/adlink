from django.contrib import admin
from models import Advert, Booked_Advert, Category, Pricing, Error_Log, Discount

class AdvertAdmin(admin.ModelAdmin):
    list_display = ('id','timestamp','title','grand_price',)    

class BookedAdvertAdmin(admin.ModelAdmin):
    list_display = ('advert','start_date','end_date','quantity','status',)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name','description',)

class PricingAdmin(admin.ModelAdmin):
    list_display = ('A1_normal_price','A2_normal_price','A3_normal_price','A4_normal_price','A5_normal_price',)

class DiscountAdmin(admin.ModelAdmin):
    list_display = ('discount_type', 'percentage', 'percentage_of',)

class Admin(admin.ModelAdmin):
    list_display = ('name','description',)

class ErrorLogAdmin(admin.ModelAdmin):
    list_display = ('problem_type','problem_message','timestamp',)


admin.site.register(Advert, AdvertAdmin)
admin.site.register(Booked_Advert, BookedAdvertAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Pricing, PricingAdmin)
admin.site.register(Discount, DiscountAdmin)
admin.site.register(Error_Log, ErrorLogAdmin)
