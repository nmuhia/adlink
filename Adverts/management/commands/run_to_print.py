import os
import json
from datetime import datetime, timedelta #, tzinfo
import datetime

import csv
import math
import pytz
from django.db.models import Q
from django.core.mail import EmailMessage
from django.utils import tzinfo
from django.utils import timezone
import sys

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils.timezone import now
from django.contrib.auth.models import User

from django.http import HttpResponse

from Adverts.models import Booked_Advert, Error_Log

import urllib, urllib2
import mimetypes
import base64

from httplib2 import Http

from oauth2client.client import SignedJwtAssertionCredentials

from apiclient.discovery import build

#for images
import Image
import ImageFont
import ImageDraw

#for word documents
from docx import Document

#for pdf documents
from pyPdf import PdfFileWriter, PdfFileReader
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A1, A2, A3, A4, A5, portrait, landscape, letter

CLOUDPRINT_URL = 'https://www.google.com/cloudprint'

submit_url = '%s/submit'%CLOUDPRINT_URL
delete_url = '%s/deletejob'%CLOUDPRINT_URL
jobs_url = '%s/jobs'%CLOUDPRINT_URL
printer_url = '%s/printer'%CLOUDPRINT_URL
search_url = '%s/search'%CLOUDPRINT_URL

printer_id = "e520d5e9-fe18-3a92-7253-6b850ccbba4f"



class Command(BaseCommand):
    def handle(self, *args, **options):
        """
            Check files ready for printing and sending request to (GCP - Google Cloud Print)
            Format: manage.py run_to_print

            ================================================================================================================
            =  The following steps were followed to add the service account to the Printer Managers
            =   1. Create a private group with a personal email address e.g. muhia.nyambura@gmail.com
            =   2. Add the service account to this private group -- NOTE: Use Add not Invite option
            =   3. When sharing a printer, add the Google Group email address with the relevant permissions
            =   4. Get printer capabilities e.g. what sizes(A3?A2?), paper type(Normal?Glossy?)
            =   5. Add the capabilities to the ticket that is being created
            ================================================================================================================
        """

        try:
            for ad in Booked_Advert.objects.all():                
                if ad.advert.advert_file != "":
                    file_name = "media/"+str(ad.advert.advert_file.name)

                    if os.path.exists(file_name) and os.path.exists != "media/":
                        f_type = mimetypes.guess_type(file_name)[0] or 'application/octet-stream'

                        #create label
                        create_label(ad.id, ad.start_date, ad.end_date)
                        
                        #append ADlink label depending on doc type
                        file_path = file_name
                        if f_type=="image/jpeg" or f_type=="image/jpg" or f_type=="image/png" or f_type=="image/gif":
                            append_label_to_image(file_path, ad.id, f_type)   

                        elif f_type ==  "application/pdf":
                            append_label_to_pdf(file_path, ad.id, f_type, ad.advert.orientation, ad.advert.size)

                        elif f_type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                            append_label_to_word(file_path, ad.id, f_type)   

                        """#send job to printer
                        title = str(ad.id)
                        copies = int(ad.quantity)                        
                        
                        b64file = Base64Encode(file_name)
                        f_data = ReadFile(b64file)
                        f_type = "dataUrl"

                        #response = print_ready_files(title, f_data, f_type, copies, ad.advert.orientation, ad.advert.size)
                        #print response['message']"""
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message="Convert Files: "+str(e))

def draw_underlined_text(draw, pos, text, font, **options):    
    twidth, theight = draw.textsize(text, font=font)
    lx, ly = pos[0], pos[1] + theight
    draw.text(pos, text, font=font, **options)
    draw.line((lx, ly, lx + twidth, ly), **options)        

def create_label(ad_id, start_date, end_date):
    try:
        img = Image.open("label.png")
        img2 = img.copy()
        draw = ImageDraw.Draw(img2)
        font = ImageFont.truetype("Aller_Lt.ttf", 13)
        draw.text((250, 3),"START: ",(0,0,0), font=font)
        draw.text((300, 3),str(start_date),(0,0,0), font=font)
        draw.text((250, 18),"END: ",(0,0,0), font=font)
        draw.text((300, 18),str(end_date),(0,0,0), font=font)
        draw.text((250, 33),"REF: ",(0,0,0), font=font)
        draw.text((300, 33),str("B%05d" % int(ad_id)),(0,0,0), font=font)
        img2.save("media/adverts/label"+str(ad_id)+".png")

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Create ADlink Label: "+str(e))


def append_label_to_word(file_name, ad_id, f_type):
    try:
        new_doc = "media/adverts/booked_ad_"+str(ad_id)+".docx"
        doc = Document(file_name)
        doc.save(new_doc)
        labeled_doc = Document(new_doc)
        labeled_doc.add_picture("media/adverts/label"+str(ad_id)+".png")
        labeled_doc.save(new_doc)

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Append ADlink Label to Word Doc: "+str(e))
    

def append_label_to_pdf(file_name, ad_id, f_type, ad_orientation, ad_size):
    try:
        label_path = "media/adverts/label"+str(ad_id)+".png"
        pdf_label_path = "media/adverts/label_"+str(ad_id)+".pdf"       
        
        width = 10
        height = 10

        c = canvas.Canvas(pdf_label_path)
        print ad_orientation
        print ad_size

        if ad_orientation == "Landscape":
            if ad_size == "A1":
                c.setPageSize(landscape(A1))
            elif ad_size == "A2":
                c.setPageSize(landscape(A2))
            elif ad_size == "A3":
                c.setPageSize(landscape(A3))
            elif ad_size == "A4":
                c.setPageSize(landscape(A4))
            elif ad_size == "A5":
                c.setPageSize(landscape(A5))

            print ad_orientation
            print landscape(str(ad_size))
            #page_size = str(landscape)+"("+str(ad_size)+")"            
            #c.setPageSize(page_size)
            #(width, height) = c.getPageSize()
        
        else:
            print ad_orientation
            print landscape(str(ad_size))
            if ad_size == "A1":
                c.setPageSize(portrait(A1))
            elif ad_size == "A2":
                c.setPageSize(portrait(A2))
            elif ad_size == "A3":
                c.setPageSize(portrait(A3))
            elif ad_size == "A4":
                c.setPageSize(portrait(A4))
            elif ad_size == "A5":
                c.setPageSize(portrait(A5))
            
            #(width, height) = c.getPageSize()

        #print canvas
        #print c
        #print ad_orientation
        #print width
        #print height

        #c.drawImage(label_path, 10, 10, mask='auto')
        #c.drawInlineImage(label_path, )
        #canvas.drawInlineImage(self, image, x,y, width=None,height=None)
        #c.drawImage(label_path, 10, 10, mask='auto')
        c.drawImage(label_path, 10, 10, mask=[0, 2, 0, 2, 0, 2, ])
        c.showPage()
        c.save()

        ad = PdfFileReader(file(file_name, 'rb')).getPage(0)        
        label = PdfFileReader(file(pdf_label_path, 'rb')).getPage(0)

        label.mergePage(ad)

        #Save the result
        output = PdfFileWriter()
        output.addPage(label)
        output.write(file("media/adverts/booked_ad_"+str(ad_id)+".pdf","w"))

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Append ADlink Label to PDF: "+str(e))


def append_label_to_image(file_name, ad_id, f_type):
    print "add label to image"
    try:
        img = Image.open(file_name)
        img2 = img.copy()

        (width, height) = img2.size

        img_label = Image.open("media/adverts/label"+str(ad_id)+".png")

        if f_type=="image/jpeg":
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".jpeg")
            img2.paste(img_label, (width - 400, 0));
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".jpeg")

        elif f_type=="image/jpg":
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".jpg")
            img2.paste(img_label, (width - 400, 0));
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".jpg")

        elif f_type=="image/png":
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".png")
            img2.paste(img_label, (width - 400, 0));
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".png")

        elif f_type=="image/gif":
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".gif")
            img2.paste(img_label, (width - 400, 0));
            img2.save("media/adverts/booked_ad_"+str(ad_id)+".gif")
                              

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Append ADlink Label to Image: "+str(e))


def return_authorized_http():
    """
        Authenticate http object for Google Cloud API access -- using service account from muhia.nyambura@gmail.com
    """    
    with open(settings.GOOGLE_PRIVATE_KEY) as f:
        private_key = f.read()

    credentials = SignedJwtAssertionCredentials(settings.GOOGLE_SERVICE_ACCOUNT_CLIENT_EMAIL, private_key,'https://www.googleapis.com/auth/cloudprint')    
    http_auth = credentials.authorize(Http())
    return http_auth


def print_ready_files(title, content, content_type, copies, orientation, size):
    """
        Check booked ads that are to be printed for posting in 12hours
    """ 
    """
    ====================================================================================
        

    In order to set the paper size you need to check which paper sizes the printer supports. You can do this by looking up the printer's 
    capabilities https://developers.google.com/cloud-print/docs/appInterfaces#printer (make sure to set use_cdd=true). Then you can find 
    the media sizes that are supported by that printer. Pick one and create a print ticket (See "ticket" parameter of https://
    developers.google.com/cloud-print/docs/appInterfaces#submit).

    If you're always printing to the same printer, then you should be able to create the ticket once, and just hardcode it in your      
    submission client.

    ====================================================================================
    """

    if orientation == "Potrait":
        set_orientation = 1
    else:
        set_orientation = 2
    try:
        ticket = {
            'version': '1.0',
            'print': {
                'vendor_ticket_item': [],
                'color': {
                    'type': 'STANDARD_MONOCHROME'
                },
                'copies': {
                    'copies': copies
                },
                #'media_size':size,
                #'page_orientation': {
                #    'type': 3
                #}
            }
        }

        body = {
            'printerid': printer_id,
            'title':"ad_file"+title,
            'ticket': ticket,
            'content' :content,
            'contentType':content_type
        } 

        response = make_request(submit_url, body)
        return response
           
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="GCP To Printer: "+str(e))

def get_printer():
    """
    Get printer details
    """
    body = {
        "printerid":printer_id
    }
    response = make_request(printer_url, body)
    return response


def get_all_printers():
    """
        Get all printers authorized by the user account
    """
    body = {
        "connection_status":"ALL"
    }
    response = make_request(search_url, body)
    return response


def get_all_jobs():
    """
        Get all jobs under this printer
    """
    body = {
        "printerid":printer_id,
    }
    response = make_request(search_url, body)
    return response



def make_request(url, body):
    """
        Call Google Cloud Print API
    """    
    try:
        http = return_authorized_http()
        headers = {'Content-type': 'application/x-www-form-urlencoded'}

        resp, response = http.request(url, "POST", headers=headers, body=urllib.urlencode(body))

        response = json.loads(str(response))

        return response
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Print Make Request: "+str(e)+" URL: "+str(url)+" Details: "+str(body))
        return "failed"


"""def SubmitJob(printerid, jobtype, jobsrc):
    Submit a job to printerid with content of dataUrl.

    Args:
        printerid: string, the printer id to submit the job to.
        jobtype: string, must match the dictionary keys in content and content_type.
        jobsrc: string, points to source for job. Could be a pathname or id string.
    Returns:
        boolean: True = submitted, False = errors.
    
    if jobtype == 'pdf':
        b64file = Base64Encode(jobsrc)
        fdata = ReadFile(b64file)
        hsid = True
    elif jobtype in ['png', 'jpeg']
        fdata = ReadFile(jobsrc)
    else:
        fdata = None

    # Make the title unique for each job, since the printer by default will name
    # the print job file the same as the title.

    datehour = time.strftime('%b%d%H%M', time.localtime())
    title = '%s%s' % (datehour, jobsrc)
    The following dictionaries expect a certain kind of data in jobsrc, depending on jobtype:
    jobtype                jobsrc
    ================================================
    pdf                    pathname to the pdf file
    png                    pathname to the png file
    jpeg                   pathname to the jpeg file
    ================================================
    
    content = {
        'pdf': fdata,
        'jpeg': jobsrc,
        'png': jobsrc,
    }
    content_type = {
        'pdf': 'dataUrl',
        'jpeg': 'image/jpeg',
        'png': 'image/png',
    }
    headers = [
        ('printerid', printerid),
        ('title', title),
        ('content', content[jobtype]),
        ('contentType', content_type[jobtype])
    ]

    files = [
        ('capabilities', 'capabilities', '{"capabilities":[]}')
    ]

    if jobtype in ['pdf', 'jpeg', 'png']:
        files.append(('content', jobsrc, fdata))
        edata = EncodeMultiPart(headers, files, file_type=content_type[jobtype])
    else:
        edata = EncodeMultiPart(headers, files)

    response = GetUrl('%s/submit' % CLOUDPRINT_URL, tokens, data=edata,
                    cookies=False)
    status = Validate(response)
    if not status:
        error_msg = GetMessage(response)
        logger.error('Print job %s failed with %s', jobtype, error_msg)

    return status"""



def GetUrl(url, tokens, data=None, cookies=False, anonymous=False):
  """Get URL, with GET or POST depending data, adds Authorization header.
    http get/post method
  Args:
    url: Url to access.
    tokens: dictionary of authentication tokens for specific user.
    data: If a POST request, data to be sent with the request.
    cookies: boolean, True = send authentication tokens in cookie headers.
    anonymous: boolean, True = do not send login credentials.
  Returns:
    String: response to the HTTP request.
  """
  request = urllib2.urlopen(url)
  if not anonymous:
    if cookies:
      logger.debug('Adding authentication credentials to cookie header')
      request.add_header('Cookie', 'SID=%s; HSID=%s; SSID=%s' % (
          tokens['SID'], tokens['HSID'], tokens['SSID']))
    else:  # Don't add Auth headers when using Cookie header with auth tokens.
      request.add_header('Authorization', 'GoogleLogin auth=%s' % tokens['Auth'])
  request.add_header('X-CloudPrint-Proxy', 'api-prober')
  if data:
    request.add_data(data)
    request.add_header('Content-Length', str(len(data)))
    request.add_header('Content-Type', 'multipart/form-data;boundary=%s' % BOUNDARY)

  # In case the gateway is not responding, we'll retry.
  retry_count = 0
  while retry_count < 5:
    try:
      result = urllib2.urlopen(request).read()
      return result
    except urllib2.HTTPError, e:
      # We see this error if the site goes down. We need to pause and retry.
      err_msg = 'Error accessing %s\n%s' % (url, e)
      logger.error(err_msg)
      logger.info('Pausing %d seconds', 60)
      time.sleep(60)
      retry_count += 1
      if retry_count == 5:
        return err_msg


def ConvertJson(json_str):
  """Convert json string to a python object.

  Args:
    json_str: string, json response.
  Returns:
    dictionary of deserialized json string.
  """
  j = {}
  try:
    j = json.loads(json_str)
    j['json'] = True
  except ValueError, e:
    # This means the format from json_str is probably bad.
    logger.error('Error parsing json string %s\n%s', json_str, e)
    j['json'] = False
    j['error'] = e

  return j

def GetKeyValue(line, sep=':'):
    """Return value from a key value pair string.

    Args:
      line: string containing key value pair.
      sep: separator of key and value.
    Returns:
      string: value from key value string.
    """
    s = line.split(sep)
    return StripPunc(s[1])

def StripPunc(s):
  """Strip puncuation from string, except for - sign.

  Args:
    s: string.
  Returns:
    string with puncuation removed.
  """
  for c in string.punctuation:
    if c == '-':  # Could be negative number, so don't remove '-'.
      continue
    else:
      s = s.replace(c, '')
  return s.strip()

def Validate(response):
  """Determine if JSON response indicated success."""
  if response.find('"success": true') > 0:
    return True
  else:
    return False

def GetMessage(response):
  """Extract the API message from a Cloud Print API json response.

  Args:
    response: json response from API request.
  Returns:
    string: message content in json response.
  """
  lines = response.split('\n')
  for line in lines:
    if '"message":' in line:
      msg = line.split(':')
      return msg[1]

  return None

def ReadFile(pathname):
  """Read contents of a file and return content.

  Args:
    pathname: string, (path)name of file.
  Returns:
    string: contents of file.
  """
  try:
    f = open(pathname, 'rb')
    try:
      s = f.read()
    except IOError, e:
      Error_Log.objects.create(problem_type="INTERNAL", problem_message="Run to Print: "+str(e))
    finally:
      f.close()
      return s
  except IOError, e:
    Error_Log.objects.create(problem_type="INTERNAL", problem_message="Run to Print: "+str(e))
    return None

def WriteFile(file_name, data):
  """Write contents of data to a file_name.

  Args:
    file_name: string, (path)name of file.
    data: string, contents to write to file.
  Returns:
    boolean: True = success, False = errors.
  """
  status = True

  try:
    f = open(file_name, 'wb')
    try:
      f.write(data)
    except IOError, e:
      logger.error('Error writing %s\n%s', file_name, e)
      status = False
    finally:
      f.close()
  except IOError, e:
    logger.error('Error opening %s\n%s', file_name, e)
    status = False

  return status

def Base64Encode(pathname):
  """Convert a file to a base64 encoded file.

  Args:
    pathname: path name of file to base64 encode..
  Returns:
    string, name of base64 encoded file.
  For more info on data urls, see:
    http://en.wikipedia.org/wiki/Data_URI_scheme
  """
  b64_pathname = pathname + '.b64'
  file_type = mimetypes.guess_type(pathname)[0] or 'application/octet-stream'
  data = ReadFile(pathname)

  # Convert binary data to base64 encoded data.
  header = 'data:%s;base64,' % file_type
  b64data = header + base64.b64encode(data)

  if WriteFile(b64_pathname, b64data):
    return b64_pathname
  else:
    return None

        

