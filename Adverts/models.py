from django.db import models
from django.contrib.auth.models import User
from Locations.models import DURATION
import datetime

import os

# Create your models here.

AD_SIZE = (
    ('A1','A1'),
    ('A2','A2'),
    ('A3','A3'),
    ('A4','A4'),
    ('A5','A5'),
)

AD_ORIENTATION = (
    ('Portrait','Portrait'),
    ('Landscape','Landscape'),
)

AD_PAPER_TYPE = (
    ('Normal','Normal'),
    ('Gloss','Gloss'),
)

AD_PAPER_EXTRA_OPTIONS = (
    ('',''),
    ('Laminate','Laminate'),
)
AD_STATUS = (
    ('Pending Payment','Pending Payment'),
    ('Ready','Ready'),
    ('Empty File', 'Empty File'),
    ('Print Failed','Print Failed'),
    ('Queued','Queued'),
    ('Printed','Printed'),
    ('Awaiting Delivery','Awaiting Delivery'),
    ('Posted','Posted'),
    ('Removed','Removed'),
    ('Payment Failed','Payment Failed'),
)

BOOKING_STAGE = (
    ('Stage1', 'Stage 1'),
    ('Stage2', 'Stage 2'),
    ('Stage3', 'Stage 3'),
)

DISCOUNT_TYPE = (
    ('by_duration','By Duration'),#based on the duration
    ('by_location','By Location'),#based on the locations
    ('by_num_of_locations','By Number of Locations'),#number of locations
)

PERCENTAGE_OF = (
    ('TP', 'Total Price'),
    ('AP', 'AD Price'),
)

class Advert(models.Model):    
    user = models.ForeignKey(User, blank=False, null=False)
    category = models.ForeignKey('Category', blank=False, null=False)
    size = models.CharField(max_length=30, choices=AD_SIZE,default="A1")
    orientation = models.CharField(max_length=30, choices=AD_ORIENTATION,default="Potrait", blank=True, null=True)
    paper_type = models.CharField(max_length=30, choices=AD_PAPER_TYPE,default="Normal", blank=True, null=True)
    paper_extra_options = models.CharField(max_length=30, choices=AD_PAPER_EXTRA_OPTIONS, default="", blank=True, null=True)
    print_ad = models.BooleanField(default=False, blank=True)
    contact_name = models.CharField(max_length=100, blank=True, null=True)
    contact_phone = models.CharField(max_length=100, blank=True, null=True)
    contact_email = models.CharField(max_length=100, blank=True, null=True)
    title = models.CharField(max_length=100, blank=False, null=False)
    description = models.CharField(max_length=100, blank=True, null=True)
    advert_file = models.FileField(upload_to='adverts', blank=True, null=True) 
    booking_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    grand_price = models.CharField(max_length=20, blank=False, null=False, default="0")
    booking_stage = models.CharField(max_length=100,choices=BOOKING_STAGE, blank=False, null=False, default="Stage1")
    timestamp = models.DateTimeField(auto_now_add=True, blank=True, null=True)


    def extension(self):
        name, extension = os.path.splitext(self.advert_file.name)
        return str(extension)
    
    class Meta:
        verbose_name = "Advert"

class Booked_Advert(models.Model):
    advert = models.ForeignKey(Advert, blank=True, null=True, default=1)
    location = models.ForeignKey('Locations.Location', blank=False, null=False)
    start_date = models.DateField()
    end_date = models.DateField()
    quantity = models.CharField(max_length=20, blank=False, null=False) 
    price = models.CharField(max_length=20, blank=False, null=False, default="0") 
    status = models.CharField(max_length=30, choices=AD_STATUS,default="Ready")

    def __unicode__(self):
        return self.advert.title

    class Meta:
        verbose_name = "Booked Advert"

class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Category"
        ordering = ['name']


class Pricing(models.Model):
    A1_normal_price = models.CharField(max_length=100, blank=True, default=0)
    A2_normal_price = models.CharField(max_length=100, blank=True, default=0)
    A3_normal_price = models.CharField(max_length=100, blank=True, default=0)
    A4_normal_price = models.CharField(max_length=100, blank=True, default=0)
    A5_normal_price = models.CharField(max_length=100, blank=True, default=0)

    A1_gloss_price = models.CharField(max_length=100, blank=True, default=0)
    A2_gloss_price = models.CharField(max_length=100, blank=True, default=0)
    A3_gloss_price = models.CharField(max_length=100, blank=True, default=0)
    A4_gloss_price = models.CharField(max_length=100, blank=True, default=0)
    A5_gloss_price = models.CharField(max_length=100, blank=True, default=0)

    A1_lamination_price = models.CharField(max_length=100, blank=True, default=0)
    A2_lamination_price = models.CharField(max_length=100, blank=True, default=0)
    A3_lamination_price = models.CharField(max_length=100, blank=True, default=0)
    A4_lamination_price = models.CharField(max_length=100, blank=True, default=0)
    A5_lamination_price = models.CharField(max_length=100, blank=True, default=0)

    class Meta:
        verbose_name = "Pricing"

class Discount(models.Model):
    discount_type = models.CharField(max_length=100, choices=DISCOUNT_TYPE)
    from_range = models.CharField(max_length=100, blank=True, null=True)
    to_range = models.CharField(max_length=100, blank=True, null=True)
    duration = models.CharField(max_length=100, choices=DURATION, default="Days")
    location = models.ForeignKey('Locations.Location', blank=True, null=True)
    threshold = models.CharField(max_length=100, blank=True, null=True)
    ad_size = models.CharField(max_length=100, blank=True, null=True)
    percentage = models.CharField(max_length=100, blank=True, null=True)
    percentage_of = models.CharField(max_length=100, choices=PERCENTAGE_OF, default="TP")
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    def __unicode__(self):
        return self.discount_type
    
    class Meta:
        verbose_name ="Discount"

class Error_Log(models.Model):
    
    ERROR_TYPES = (
        ("SEND_SMS","Send SMS Failed"),
        ("SEND_EMAIL","Send EMAIL Failed"),
        ("INTERNAL","Other Internal Error"),
        ("EXTERNAL","Other External Error"),
        ("KK_PROCESS","Payment Processing Error"),
        ("FAULTY_DATA","Faulty Data Warning"),
        ("DEBUGGING", "Debugging Message"),
        ("LOG_DATA", "Log Data for Tracking"),
        ("PING_ERROR", "Server URL Doesn't Exist"),
        ("WARNING", "Warning / Alert"),
        ("OTHER","Other Error: Unknown Type"),
    )
    

    timestamp = models.DateTimeField(auto_now_add=True)
    problem_type = models.CharField(max_length=30, choices=ERROR_TYPES,
                                    default="OTHER")
    problem_message = models.CharField(max_length=1000, blank=True, null=True)
    creator = models.CharField(max_length=50, blank=True, null=True)
    
    is_resolved = models.BooleanField(default=False)
    advert = models.ForeignKey(Advert, blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return self.problem_type + "; " + self.problem_message
    
    class Meta:
        verbose_name = "Error Log"
        ordering = ['-timestamp']

