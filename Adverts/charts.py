from datetime import date, timedelta, datetime

from django.http import HttpResponse
from django.utils import timezone
from django.utils.timezone import now,utc

from Adverts.models import Advert, Booked_Advert
from Payments.models import Payment

def get_booked_ads_data_chart(start_date=None, end_date=None):
    """
        Returns chart data for selected dates
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7

    ret_dict = {
        'total': [],
        'dates':[]
    }

    if start_date is None:
        end_date = now().replace(hour=0, minute=0, second=0, microsecond=0)

    else: 
        start_date = datetime.strptime(start, "%Y-%m-%d")
        end_date = datetime.strptime(end, "%Y-%m-%d")
        start_date = timezone.make_aware(start_date,timezone=utc)
        end_date = timezone.make_aware(end_date,timezone=utc)
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        total = 0
        next_day = end_date - timedelta(days=i) 
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        
        ads = Advert.objects.filter(booking_date__gte=next_day, booking_date__lt=next_day+timedelta(days=1))
        if len(ads) > 0:
            total = ads.count()
        ret_dict['total'].append(total)

    return ret_dict        
